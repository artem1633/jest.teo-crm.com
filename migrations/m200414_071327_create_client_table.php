<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%client}}`.
 */
class m200414_071327_create_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%client}}', [
            'id' => $this->primaryKey(),
            'official_name' => $this->string()->comment('Наименование'),
            'address' => $this->text()->comment('Адрес'),
            'contact_person' => $this->string()->comment('Контактное лицо'),
            'phone_number' => $this->integer()->comment('Телефон'),
            'email' => $this->string()->comment('Email'),
            'bank_details_file' => $this->string()->comment('Файл с реквизитами')
        ]);

        $this->addForeignKey(
            'fk-users-client_id',
            '{{%users}}',
            'client_id',
            '{{%client}}',
            'id',
            'CASCADE', //Если удаляется клиент - удаляем и его пользователей
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-users-client_id', '{{%users}}');
        $this->dropTable('{{%client}}');
    }
}
