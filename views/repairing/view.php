<?php

use app\models\Apparatus;
use app\models\ApparatusToClient;
use app\models\constants\UsersConstants;
use app\models\Repairing;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Repairing */
?>
<div class="repairing-view">
 
    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'created_at:datetime',
                [
                    'attribute' => 'client_id',
                    'value' => $model->client ? $model->client->official_name : null,

                ],
                [
                    'attribute' => 'apparatus_id',
                    'value' => function (Repairing $model) {
                        /** @var Apparatus $apparatus */
                        $apparatus = $model->apparatus ?? null;
                        /** @var \app\models\Client $client */
                        $client = $model->client;
                        $str = '';
                        if ($apparatus) {
                            $str = $apparatus->getApparatusFullName() ?? '';
                        }

                        if ($client) {
                            $str .= ' №' . ApparatusToClient::findOne($model->apparatus_id)->serial_number ?? null;
                        }

                        return $str;
                    },

                ],
                'counter_1',
//                [
//                    'attribute' => 'counter_2',
//                    'visible' => $model->apparatus->type === Apparatus::TYPE_COLORED,
//                ],
//                [
//                    'attribute' => 'counter_3',
//                    'visible' => $model->apparatus->type === Apparatus::TYPE_COLORED,
//                ],
                [
                    'attribute' => 'description',
                ],
                [
                    'attribute' => 'done_at',
                    'visible' => UsersConstants::isAdmin(),
                    'format' => 'datetime'
                ],
                [
                    'attribute' => 'photos',
                    'value' => function (Repairing $model) {
                        return $model->getListOfPhoto();
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'status',
                    'value' => $model->getStatusName($model->status),
                ],
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), '_error');
        echo $e->getMessage();
    } ?>

</div>
