<?php

namespace app\models;

use app\models\query\ApparatusToClientQuery;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "apparatus_to_client".
 *
 * @property int $id
 * @property int $apparatus_id Аппарат
 * @property int $client_id Клиент
 * @property string $notes Примечание
 * @property int $is_owner Является ли клиент собственником аппарата
 * @property string $serial_number Серийный номер аппарата
 * @property string $brand Марка аппарата
 * @property mixed $apparatus_info Марка модель серийный номер
 *
 * @property Apparatus $apparatus
 * @property Client $client
 * @property Report[] $reports
 */
class ApparatusToClient extends ActiveRecord
{
    public $brand;
    public $apparatus_info;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'apparatus_to_client';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['apparatus_id', 'client_id', 'is_owner'], 'integer'],
            [['notes'], 'string'],
            [['serial_number'], 'string', 'max' => 255],
            [
                ['apparatus_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Apparatus::className(),
                'targetAttribute' => ['apparatus_id' => 'id']
            ],
            [
                ['client_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Client::className(),
                'targetAttribute' => ['client_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'apparatus_id' => 'Аппарат',
            'client_id' => 'Клиент',
            'notes' => 'Примечание',
            'is_owner' => 'Собственник',
            'serial_number' => 'Серийный номер',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApparatus()
    {
        return $this->hasOne(Apparatus::className(), ['id' => 'apparatus_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

    /**
     * {@inheritdoc}
     * @return ApparatusToClientQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ApparatusToClientQuery(get_called_class());
    }

    /**
     * Получает список аппартов для клиента (Марка модель серйный номер)
     * @param integer $id Идентификатор клиента
     * @return array
     */
    public function getApparatusListForClient($id = null)
    {
        if (!$id) {
            /** @var Users $identity */
            $identity = Yii::$app->user->identity;
            $client_id = $identity->client_id;
        } else {
            $client_id = $id;
        }


        $sql = <<<SQL
        SELECT at.id, CONCAT(b.name, ' ', a.model, ' №', at.serial_number) as apparatus_info FROM apparatus_to_client at
        LEFT JOIN apparatus a ON at.apparatus_id = a.id
        LEFT JOIN brand b ON a.brand_id = b.id
        WHERE at.client_id = $client_id
        ORDER BY (b.name)
SQL;

        Yii::info($sql, 'test');

        return ArrayHelper::map(ApparatusToClient::findBySql($sql)->all(), 'id', 'apparatus_info');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReports()
    {
        return $this->hasMany(Report::class, ['apparatus_to_client_id' => 'id']);
    }
}
