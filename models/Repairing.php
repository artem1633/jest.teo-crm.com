<?php

namespace app\models;

use app\components\EmailHelper;
use app\models\constants\UsersConstants;
use app\models\query\RepairingQuery;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\UploadedFile;


/**
 * This is the model class for table "repairing".
 *
 * @property int $id
 * @property string $created_at
 * @property int $client_id Клиент
 * @property int $apparatus_id Аппарат
 * @property int $counter_1 Счетчик 1
 * @property int $counter_2 Счетчик 2
 * @property int $counter_3 Счетчик 3
 * @property string $description Описание инцидента
 * @property string $photos Путь к папке с фото
 * @property string $done_at Дата закрытия заявки
 * @property int $status Статус заявки
 *
 * @property Apparatus $apparatus
 * @property ApparatusToClient $apparatusToClient
 * @property Client $client
 */
class Repairing extends ActiveRecord
{
    const STATUS_DRAFT = 1;
    const STATUS_NEW = 2;
    const STATUS_WORK = 3;
    const STATUS_DONE = 4;

    const SCENARIO_COLORED = 'colored';
    const SCENARIO_MONOCHROME = 'monochrome';

    public $images;
    public $client_name;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'repairing';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'done_at'], 'safe'],
            [['client_id', 'apparatus_id', 'counter_1', 'counter_2', 'counter_3', 'status'], 'integer'],
            [['description'], 'string', 'max' => 300],
            [['photos'], 'string', 'max' => 255],
            [
                ['apparatus_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => ApparatusToClient::className(),
                'targetAttribute' => ['apparatus_id' => 'id']
            ],
            [
                ['client_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Client::className(),
                'targetAttribute' => ['client_id' => 'id']
            ],
            [['apparatus_id'], 'required', 'message' => 'Необходимо выбрать аппарат'],
            [['apparatus_id', 'counter_1', 'counter_2', 'counter_3'], 'required', 'on' => self::SCENARIO_COLORED],
            [['apparatus_id', 'counter_1'], 'required', 'on' => self::SCENARIO_MONOCHROME],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Дата создания',
            'client_id' => 'Клиент',
            'apparatus_id' => 'Аппарат',
            'counter_1' => 'Счетчик Общий',
            'counter_2' => 'Счетчик Черный',
            'counter_3' => 'Счетчик Цветной',
            'description' => 'Описание инцидента',
            'photos' => 'Фото/сканы отпечатков',
            'done_at' => 'Дата закрытия заявки',
            'status' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApparatusToClient()
    {
        return $this->hasOne(ApparatusToClient::className(), ['id' => 'apparatus_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApparatus()
    {
        return $this->hasOne(Apparatus::className(), ['id' => 'apparatus_id'])
            ->via('apparatusToClient');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

    /**
     * {@inheritdoc}
     * @return RepairingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RepairingQuery(get_called_class());
    }

    /**
     * Массив статусов и наименований
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_DRAFT => 'Черновик',
            self::STATUS_NEW => 'Новая',
            self::STATUS_WORK => 'В работе',
            self::STATUS_DONE => 'Выполнено',
        ];
    }

    /**
     * Возвращает наименование статуса по ID
     * @param int $status Статус
     * @return string
     */
    public function getStatusName($status)
    {
        return $this->getStatusList()[$status];
    }

    /**
     * Загрузка фоток/сканов отпечатанных страниц
     * @return bool
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    public function uploadPrintPhoto()
    {
        $form = new UploadForm();
        $form->scenario = $form::SCENARIO_SCAN;

        //Фотки счетчиков храним uploads/prints/ID_клиента/Номерзавки/Номерсчетчика.jpg
        $target_dir = 'uploads/prints/' . $this->client_id . '/' . $this->id;
        $fileNames = [];
        Yii::info('Целевая папка: ' . $target_dir, 'test');

        $form->files = UploadedFile::getInstances($this, 'images');
        Yii::info($form->files, 'test');

        $file_name = 'print';
        if ($form->files) {
            Yii::info('Имя файла: ' . $file_name, 'test');
            $fileNames[] = $form->upload($target_dir, $file_name); //$fileNames[] пока не исползуется
        } else {
            Yii::info('Файлы не агружены', 'test');
        }
        Yii::info($fileNames, 'test');

        if ($fileNames) {
            Yii::$app->db
                ->createCommand()
                ->update('repairing', ['photos' => $target_dir], ['id' => $this->id])
                ->execute();
        }
        return true;
    }

    /**
     * Возвращает список ссылок на фотки счетчиков (для отображния например в карточке заявки)
     * @param string $separator Разделитель строк
     * @param int $target_counter_num Номер счетчика (0, 1, 2 или 3) Ноль - возвращать все файлы
     * @return string|array
     */
    public function getListOfPhoto($separator = PHP_EOL, $target_counter_num = 0)
    {
        Yii::info('List of Photo function', 'test');

        if (!is_dir($this->photos)) {
            return false;
        }
        $dir = $this->photos;

        $files = array_diff(scandir($dir), ['..', '.', '.gitignore']);

        $prepared_photos = [];
        Yii::info($files, 'test');

        foreach ($files as $file_name) {
            $path_file = $dir . '/' . $file_name;
            $img = Html::img('/' . $path_file, [
                'style' => 'max-width: 100px',
                'alt' => $file_name,
            ]);
            if (is_file($path_file)) {
                if ($target_counter_num == 0) {
//                    $prepared_photos[] = Html::a($file_name, ['download', 'file' => $path_file]) . $separator;
                    $prepared_photos[] = Html::a($img, ['/' . $path_file], [
                            'data-lightbox' => $file_name
                        ]) . $separator;
                } else {
                    $counter_num = explode('_', $file_name)[0];
//                    $prepared_photos[$counter_num][] = Html::a($file_name,
//                            ['download', 'file' => $path_file]) . $separator;
                    $prepared_photos[$counter_num][] = Html::a($img,
                            ['/' . $path_file], [
                                'data-lightbox' => $file_name
                            ]) . $separator;
                }
            } else {
                Yii::warning('Пропущено. Файл: ' . $path_file, 'test');
                continue;
            }
        }

        Yii::info($prepared_photos, 'test');

        if ($target_counter_num == 0) {
            return implode('', $prepared_photos);
        } else {
            return implode('', $prepared_photos[$target_counter_num]);
        }
    }

    /**
     * Отправка почты о новом заказе админу
     * @return void
     * @throws \HttpInvalidParamException
     */
    public function sendNotify()
    {
        $url = Url::to(['/repairing/view', 'id' => $this->id], true);

        $email = new EmailHelper();
        $email->email_type = EmailHelper::TYPE_REPAIRING;
        $email->subject = $this->client->official_name . ', ' . $this->apparatus->getApparatusFullName() . ', инцидент!';

        $email->html_body = '<br><p>Для перехода в заявку нажмите ' . Html::a('здесь',
                $url) . '. Или скопируйте в адресную строку ссылку: ' . $url . ' </p>';

        $email->send();
    }

    /**
     * Возвращает ярлычок с количеством новых инцидентов
     * @return string
     */
    public function getLabelCounts()
    {
        if (!UsersConstants::isAdmin()) {
            return '';
        }

        $new_repairing = self::find()->andWhere([
            'status' => self::STATUS_NEW
        ])->count();

        if ($new_repairing > 0) {
            return "<span class='label label-warning' style='margin-left: 5px;' title='Новые инциденты'>{$new_repairing}</span>";
        }

        return '';
    }

    /**
     * Возвращает кнопку для смены статуса заявки на РМ
     * @return null|string
     */
    public function getButton()
    {
        $status = $this->status + 1;

        if ($status > self::STATUS_DONE) {
            return null;
        }

        if ($status == self::STATUS_WORK) {
            return Html::a('В работу', ['/repairing/change-status', 'id' => $this->id, 'status' => $status], [
                'class' => 'btn btn-info btn-sm',
                'title' => 'Заявку в работу',
                'data-toggle' => 'tooltip',
                'role' => 'modal-remote',
                'data-confirm-title' => 'Вы уверены?',
                'data-confirm-message' => 'Сменить статус заявки',
                'data-confirm-ok' => 'В работу',
                'data-confirm-cancel' => 'Отмена',
            ]);
        } elseif ($status == self::STATUS_DONE) {
            return Html::a('Выполнено', ['/repairing/change-status', 'id' => $this->id, 'status' => $status], [
                'class' => 'btn btn-info btn-sm',
                'title' => 'Завершить заявку',
                'data-toggle' => 'tooltip',
                'role' => 'modal-remote',
                'data-confirm-title' => 'Вы уверены?',
                'data-confirm-message' => 'Завершить заявку',
                'data-confirm-ok' => 'Завершить',
                'data-confirm-cancel' => 'Отмена',
            ]);
        }
        return null;
    }

    /**
     * @return string
     */
    public function getCountersPhoto()
    {
        $photo_path = $this->photos;
        Yii::info(pathinfo($this->photos), 'test');
        $photo_name = pathinfo($this->photos)['filename'];

        return <<<HTML
    <a data-lightbox="{$photo_name}" href="/{$photo_path}">
        <img style="max-width: 100px" src="/{$photo_path}" alt="{$photo_name}">
    </a>
HTML;
    }

}
