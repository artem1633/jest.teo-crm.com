<?php

use app\models\constants\UsersConstants;
use app\models\Users;
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'client_id',
        'content' => function (Users $model) {
            return Html::a($model->client->official_name ?? '', ['/client/view', 'id' => $model->client->id ?? ''], [
                'role' => 'modal-remote'
            ]);
        }
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'avatar',
//        'filter' => false,
//        'contentOptions' => ['style'=>'width: 48px; height:48px;'],
//        'content' => function (Users $data) {
//         return Html::img($data->getAvatar(), [ 'style' => 'height:48px;width:48px; object-fit: cover;']);
//
//        },
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'fio',
        'content' => function (Users $model) {
            return Html::a($model->fio, ['/users/view', 'id' => $model->id], [
                'role' => 'modal-remote'
            ]);
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'login',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'role',
        'filter' => UsersConstants::getArray(),
        'content' => function (Users $data) {
            return $data->getTypeDescription();
        },
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'template' => '{update} {leadDelete}',
        'buttons' => [
            'leadDelete' => function ($url, $model) {
                if ($model->id != 1) {
                    $url = Url::to(['/users/delete', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                        'role' => 'modal-remote',
                        'title' => 'Удалить',
                        'data-confirm' => false,
                        'data-method' => false,// for overide yii data api
                        'data-request-method' => 'post',
                        'data-toggle' => 'tooltip',
                        'data-confirm-title' => 'Подтвердите действие',
                        'data-confirm-message' => 'Вы уверены что хотите удалить этого элемента?',
                        'class' => UsersConstants::isSuperAdmin() ? '' : 'hidden',
                    ]);
                }
                return false;
            },
        ],
        'urlCreator' => function ($action, $model, $key) {
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'Просмотр', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Изменить', 'data-toggle' => 'tooltip'],
        'deleteOptions' => [
            'role' => 'modal-remote',
            'title' => 'Удалить',
            'data-confirm' => false,
            'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Are you sure?',
            'data-confirm-message' => 'Are you sure want to delete this item'
        ],
    ],

];   