<?php

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $order_to_cons app\models\OrderToConsumables */
/* @var $order_to_cons_models app\models\OrderToConsumables[] */
?>
<div class="order-update">

    <?= $this->render('_form', [
        'model' => $model,
        'order_to_cons' => $order_to_cons,
        'order_to_cons_models' => $order_to_cons_models,
    ]) ?>

</div>
