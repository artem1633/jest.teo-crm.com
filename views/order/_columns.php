<?php

use app\models\ApparatusToClient;
use app\models\constants\UsersConstants;
use app\models\Order;
use yii\helpers\Html;
use yii\helpers\Url;

return [
//    [
//        'class' => 'kartik\grid\CheckboxColumn',
//        'width' => '20px',
//    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'created_at',
        'format' => 'datetime',
        'vAlign' => 'middle',
        'contentOptions' => [
            'class' => 'order-column',
            'style' => 'cursor: pointer;'
        ]
    ],
//    [
//        'class' => '\kartik\grid\DataColumn',
//        'attribute' => 'id',
//        'label' => '№',
//        'width' => '100px',
//        'vAlign' => 'middle',
//        'hAlign' => 'center',
//    ],

    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'client_id',
        'content' => function (Order $model) {
            return $model->client->official_name ?? null;
        },
        'vAlign' => 'middle',
        'visible' => UsersConstants::isAdmin(),
        'contentOptions' => [
            'class' => 'order-column',
            'style' => 'cursor: pointer;'
        ]
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'apparatus_id',
        'content' => function (Order $model) {
            /** @var \app\models\Apparatus $apparatus */
            $apparatus = $model->apparatus ?? null;
            /** @var \app\models\Client $client */
            $client = $model->client;
            $str = '';
            if ($apparatus) {
                $str = $apparatus->getApparatusFullName() ?? '';
            }

            if ($client) {
                $str .= ' №' . ApparatusToClient::findOne($model->apparatus_id)->serial_number ?? null;
            }

            return $str;
        },
        'vAlign' => 'middle',
        'contentOptions' => [
            'class' => 'order-column',
            'style' => 'cursor: pointer;'
        ]
    ],

    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'counter_1',
        'hAlign' => 'center',
        'vAlign' => 'middle',
        'contentOptions' => [
            'class' => 'order-column',
            'style' => 'cursor: pointer;'
        ]
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'consumables',
        'content' => function (Order $model) {
            return $model->getConsumablesList(false, ',<br>');
        },
        'hAlign' => 'center',
        'vAlign' => 'middle',
        'contentOptions' => [
            'class' => 'order-column',
            'style' => 'cursor: pointer;'
        ]
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'counter_2',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'counter_3',
//    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'photos',
    // ],

    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'done_at',
        'format' => 'datetime',
        'hAlign' => 'center',
        'vAlign' => 'middle',
        'contentOptions' => [
            'class' => 'order-column',
            'style' => 'cursor: pointer;'
        ]
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'status',
        'filter' => (new Order())->getStatusList(),
        'content' => function (Order $model) {
            $status_name = $model->getStatusName($model->status);
            if ($model->status == $model::STATUS_DRAFT && $model->client_id == Yii::$app->user->identity->client_id) {
                $btn = Html::a('Заказать', ['to-new', 'id' => $model->id], [
                    'class' => 'btn btn-small btn-success',
                    'title' => 'Отправить заказ',
                    'data-toggle' => 'tooltip',
                    'role' => 'modal-remote',
                    'data-confirm-title' => 'Отправить заказ диспетчеру?',
                    'data-confirm-message' => 'Подтвердите заказ',
                    'data-confirm-ok' => 'Отправить заказ',
                    'data-confirm-cancel' => 'Отмена',
                ]);
            } elseif (UsersConstants::isAdmin() && $model->status > $model::STATUS_DRAFT) {
                //Для админа показываем кнопки в работу или выполнено
                $btn = $model->getButton();
            } else {
                $btn = '';
            }

            return '<p class="margin-bottom: 0.5rem;"><b>' . $status_name . '</b></p>' . $btn;
        },
        'format' => 'raw',
        'hAlign' => 'center',
        'vAlign' => 'middle',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key) {
            return Url::to(['/order/' . $action, 'id' => $key]);
        },
        'buttons' => [
            'update' => function ($url, Order $model) {
                if ($model->status === $model::STATUS_DRAFT || UsersConstants::isAdmin()) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                        'role' => 'modal-remote',
                        'title' => 'Редактировать',
                        'data-toggle' => 'tooltip',
                    ]);
                } else {
                    return null;
                }
            },
            'delete' => function ($url, Order $model) {
                if ($model->status === $model::STATUS_DRAFT || UsersConstants::isSuperAdmin()) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                        'role' => 'modal-remote',
                        'title' => 'Удалить заказ',
                        'data-confirm' => false,
                        'data-method' => false,// for overide yii data api
                        'data-request-method' => 'post',
                        'data-toggle' => 'tooltip',
                        'data-confirm-title' => 'Вы уверены?',
                        'data-confirm-message' => 'Подтвердите удаление заказа',
                        'data-confirm-ok' => 'Удалить',
                        'data-confirm-cancel' => 'Отмена',
                    ]);
                } else {
                    return null;
                }
            }
        ],
        'viewOptions' => [
            'role' => 'modal-remote',
            'title' => 'Просмотр',
            'data-toggle' => 'tooltip',
            'class' => 'view-repairing'
        ],
//        'updateOptions' => [
//            'role' => 'modal-remote',
//            'title' => 'Update',
//            'data-toggle' => 'tooltip',
//        ],
//        'deleteOptions' => [
//            'role' => 'modal-remote',
//            'title' => 'Удалить заказ',
//            'data-confirm' => false,
//            'data-method' => false,// for overide yii data api
//            'data-request-method' => 'post',
//            'data-toggle' => 'tooltip',
//            'data-confirm-title' => 'Вы уверены?',
//            'data-confirm-message' => 'Подтвердите удаление заказа',
//            'data-confirm-ok' => 'Удалить',
//            'data-confirm-cancel' => 'Отмена',
//        ],
    ],

];   