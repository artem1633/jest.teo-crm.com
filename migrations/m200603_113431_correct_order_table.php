<?php

use app\models\ApparatusToClient;
use app\models\Order;
use yii\db\Migration;

/**
 * Class m200603_113431_correct_order_table
 */
class m200603_113431_correct_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $orders = Order::find()->all();
        Yii::info('Orders count: ' . count($orders), 'test');

        /** @var Order $order */
        foreach ($orders as $order) {
            //Ищем аппарат клиента
            $atc = ApparatusToClient::find()->andWhere([
                'apparatus_id' => $order->apparatus_id,
                'client_id' => $order->client_id
            ])->one();

            if ($atc){
                //меняем ID аппарата на ID аппарата клиента
                $order->apparatus_id = $atc->id;
                $order->save(false);
            }

        }

        $this->addForeignKey(
            'fk-order-apparatus_to_client_id',
            '{{%order}}',
            'apparatus_id',
            '{{%apparatus_to_client}}',
            'id',
            'SET NULL', //При удалении аппарата заявку не удаляем
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200603_113431_correct_order_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200603_113431_correct_order_table cannot be reverted.\n";

        return false;
    }
    */
}
