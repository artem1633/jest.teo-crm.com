<?php

use app\models\Client;
use app\models\constants\UsersConstants;
use yii\helpers\Html;
use yii\helpers\Url;

return [
//    [
//        'class' => 'kartik\grid\CheckboxColumn',
//        'width' => '20px',
//    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'official_name',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'contact_person',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'phone_number',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'email',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Информация',
        'content' => function(Client $model){
            $app_count = count($model->apparatusToClients ?? []);
            $user_count = $model->countUsers;
            return Html::a('Аппаратов: ' . $app_count, ['/apparatus-to-client/index', 'client_id' => $model->id], [
                'role' => 'modal-remote'
            ]) . '<br>' .
                Html::a('Пользователей: ' . $user_count, ['/users/index', 'client_id' => $model->id], [
                    'role' => 'modal-remote'
                ])  ;
        }
    ],
//    [
//        'class' => '\kartik\grid\DataColumn',
//        'content' => function (Client $model) {
//            $users_info = 'Пользователей: ' . $model->countUsers;
//            return $users_info . '<br>' . '<br>' . Html::a('Создать пользователя', ['create-user', 'id' => $model->id],
//                    [
//                        'class' => 'btn btn-info btn-sm',
//                        'role' => 'modal-remote',
//                        'data-confirm-title' => 'Вы уверены?',
//                        'data-confirm-message' => 'Подтвердите создание пользователя',
//                        'data-confirm-ok' => 'Создать',
//                        'data-confirm-cancel' => 'Отмена',
//                    ]);
//        }
//    ],

    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'template' => '{add_client}&nbsp;{add_apparatus} <br> {view} {update} {delete}',
        'urlCreator' => function ($action, $model, $key) {
            return Url::to([$action, 'id' => $key]);
        },
        'buttons' => [
            'add_apparatus' => function ($url, Client $model) {
                return Html::a('<i class="fa fa-plus"></i>', ['/apparatus-to-client/create', 'id' => $model->id], [
                    'title' => 'Добавить аппарат',
                    'data-toggle' => 'tooltip',
                    'role' => 'modal-remote'
                ]);
            },
            'add_client' => function ($url, Client $model) {
                return Html::a('<i class="fa fa-user"></i>', ['create-user', 'id' => $model->id], [
                    'title' => 'Добавить ползователя',
                    'data-toggle' => 'tooltip',
                    'role' => 'modal-remote',
                    'data-confirm-title' => 'Вы уверены?',
                    'data-confirm-message' => 'Подтвердите создание пользователя',
                    'data-confirm-ok' => 'Создать',
                    'data-confirm-cancel' => 'Отмена',
                ]);
            }

        ],
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'Просмотр', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Редактировать', 'data-toggle' => 'tooltip'],
        'deleteOptions' => [
            'role' => 'modal-remote',
            'title' => 'Удалить',
            'data-confirm' => false,
            'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Are you sure?',
            'data-confirm-message' => 'Are you sure want to delete this item',
            'class' => UsersConstants::isSuperAdmin() ? '' : 'hidden'
        ],
    ],

];   