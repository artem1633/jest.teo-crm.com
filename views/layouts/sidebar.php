<?php

use app\models\constants\UsersConstants;
use app\models\Users;
use yii\helpers\Html;
use app\widgets\Menu;

$pathInfo = Yii::$app->request->pathInfo;

/** @var Users $identity */
$identity = Yii::$app->user->identity ?? null;

$order_label = (new \app\models\Order())->getLabelCounts();
$repairing_label = (new \app\models\Repairing())->getLabelCounts();
//$zayavki_label = ($order_label || $repairing_label) ?  "<span class='fa fa-warning' style='margin-left: 5px;' title='Новые заявки'></span>": '';


?>

<div id="sidebar" class="sidebar sidebar-transparent">
    <!-- begin sidebar scrollbar -->
    <div data-scrollbar="true" data-height="100%">
        <!-- begin sidebar user -->

        <ul class="nav">
            <?php if (isset(Yii::$app->user->identity)) { ?>
                <li class="nav-profile">
                    <a href="javascript:" data-toggle="nav-profile">
                        <div class="cover with-shadow"></div>
                        <div class="image">
                            <img src="<?= $identity->getAvatar() ?>" alt=""/>
                        </div>
                        <div class="info">
                            <b class="caret pull-right"></b>
                            <?= $identity->fio ?>
                            <small><?= $identity->getTypeDescription() ?></small>
                        </div>
                    </a>
                </li>
            <?php } ?>
            <li>
                <ul class="nav nav-profile">
                    <li><?= Html::a('<i class="fa fa-cogs"></i> Изменить Профиль', ['/users/profile'], []); ?></li>
                    <li><?= Html::a(
                            '<i class="fa fa-sign-out"></i>Выйти',
                            ['/site/logout'],
                            ['data-method' => 'post',]
                        ) ?></li>
                </ul>
            </li>
        </ul>
        <!-- end sidebar user -->
        <!-- begin sidebar nav -->

        <?php
        try {
            echo Menu::widget(
                [
                    'options' => ['class' => 'nav'],
                    'items' => [
                        [
                            'label' => 'Инциденты' . $repairing_label,
                            'encode' => false,
                            'icon' => 'wrench',
                            'url' => ['/repairing',],
                            'active' => $this->context->id == 'repairing'
                        ],
                        [
                            'label' => 'Заказы РМ' .  $order_label,
                            'icon' => 'cubes',
                            'encode' => false,
                            'url' => ['/order',],
                            'active' => $this->context->id == 'order'
                        ],
                        [
                            'label' => 'Клиенты',
                            'icon' => 'users',
                            'url' => ['/client'],
                            'active' => $this->context->id == 'client',
                            'visible' => $identity ? ($identity->role == 1 ? true : false) : false,
                        ],
                        [
                            'label' => 'Справочники',
                            'icon' => 'book',
                            'url' => ['#'],
                            'visible' => UsersConstants::isAdmin(),
                            'items' => [
                                [
                                    'label' => 'Аппараты',
                                    'icon' => 'print',
                                    'url' => ['/apparatus',],
                                    'active' => $this->context->id == 'apparatus'
                                ],
                                [
                                    'label' => 'Расходники',
                                    'icon' => 'cog',
                                    'url' => ['/consumables',],
                                    'active' => $this->context->id == 'consumables'
                                ],
                                [
                                    'label' => 'Пользователи',
                                    'icon' => 'users',
                                    'url' => ['/users'],
                                    'active' => $this->context->id == 'users'
                                ],

                            ],
                        ],
                        [
                            'label' => 'Статистика',
                            'icon' => 'bar-chart',
                            'url' => ['/site/statistics'],
                            'active' => $this->context->id == 'stat' || $this->context->id == 'site',
                            'visible' => UsersConstants::isAdmin(),
                        ],
                        [
                            'label' => 'Ежемесячные показания счетчиков',
                            'icon' => 'tachometer',
                            'url' => ['/report'],
                            'active' => $this->context->id == 'report',
                        ],
                        [
                            'label' => 'Настройки',
                            'icon' => 'cog',
                            'url' => ['/settings'],
                            'active' => $this->context->id == 'settings',
                            'visible' => UsersConstants::isAdmin(),
                        ],
                        [
                            'label' => 'Помощь',
                            'icon' => 'question-circle',
                            'url' => ['/site/help'],
                            'active' => $this->context->id == 'settings',
                            'visible' => !UsersConstants::isAdmin(),
                        ],
                    ],
                ]
            );
        } catch (Exception $e) {
            Yii::error($e->getMessage(), '_error');
            echo $e->getMessage();
        } ?>
        <li style="list-style: none;"><a href="javascript:" class="sidebar-minify-btn" data-click="sidebar-minify"><i
                        class="fa fa-angle-double-left"></i></a></li>
        <!-- end sidebar nav -->
    </div>
    <!-- end sidebar scrollbar -->
</div>
<div class="sidebar-bg"></div>