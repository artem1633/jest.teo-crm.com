<?php

use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var $model \app\models\Users */
?>

<div class="users-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-4">
            <div id="image" class="col-md-12 text-center">
                <?= Html::img($model->getAvatar(), [
                    'style' => 'width:150px; height:150px;object-fit: cover;',
                    'class' => 'img-circle',
                ]) ?>
            </div>
            <div class="col-md-12" style="margin-top: 2rem;">
                <?php try {
                    echo $form->field($model, 'image')->widget(FileInput::class, [
                        'options' => [
                            'accept' => 'image/*',
                            'multiple' => false,
                            'class' => 'image_input',
                            'id' => 'inputFile',
                        ],
                        'pluginOptions' => [
                            'showPreview' => false,
                            'showCaption' => false,
                            'showRemove' => false,
                            'showUpload' => false,
                            'browseClass' => 'btn btn-primary btn-block btn-sm',
                            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                            'browseLabel' => 'Выберите фото',
                            'allowedFileExtensions' => ['jpeg', 'jpg', 'png', 'gif'],
                        ]
                    ])->label(false);
                } catch (Exception $e) {
                    Yii::error($e->getMessage(), '_error');
                    echo $e->getMessage();
                } ?>
            </div>
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'role')->dropDownList($model->getRoles(), ['prompt' => 'Выберите роль']) ?>
                </div>
                <div class="col-md-6">
                    <?= $model->isNewRecord ? $form->field($model,
                        'password')->textInput(['maxlength' => true]) : $form->field($model,
                        'new_password')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p>Пользователь клиента:&nbsp;<b><?= $model->client ? $model->client->official_name : '' ?></b></p>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php
$this->registerJs(<<<JS

$("#inputFile").on('change',function(e){
  var files = e.target.files;
    $.each(files, function(i,file){
        var reader = new FileReader();
        var image = $('#image');
        reader.readAsDataURL(file);
        reader.onload = function(e){
            var template = '<img style="object-fit: cover; width:150px; height:150px;" src="'+e.target.result+'" class="img-circle"> ';
            image.html('');
            image.append(template);
        };
    });
});
JS
);
?>

