<?php

use app\models\constants\UsersConstants;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ApparatusToClientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Apparatus To Clients';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);
$export = UsersConstants::isSuperAdmin() ? '{export}' : '';

?>
<div class="apparatus-to-client-index">
    <div id="ajaxCrudDatatable">
        <?php
        try {
            echo GridView::widget([
                'id' => 'crud-datatable',
                'dataProvider' => $dataProvider,
//                'filterModel' => $searchModel,
                'pjax' => true,
                'columns' => require(__DIR__ . '/_columns.php'),
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'toolbar' => [
                    [
                        'content' =>
                            '{toggleData}' .
                            $export
                    ],
                ],
                'panel' => [
                    'type' => 'inverse',
                    'heading' => '<i class="glyphicon glyphicon-list"></i> Спсисок аппаратов',
                    //                'before'=>'<em>* Resize table columns just like a spreadsheet by dragging the column edges.</em>',
                    'after' => '<div class="clearfix"></div>',
                ]
            ]);
        } catch (Exception $e) {
            Yii::error($e->getMessage(), '_error');
            echo $e->getMessage();
        } ?>
    </div>
</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
