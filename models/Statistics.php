<?php

namespace app\models;

use yii\base\Model;

/**
 * @property string $date_start Начало периода
 * @property string $date_end Конец периода
 * @property integer $client_id Идентификатор клинта
 * @property integer $status Статус заказа/заявки
 * @property integer $on_created_at Фильтрация по дате создания, если = 0 то по дате выполнения
 */

class Statistics extends Model
{
    public $date_start;
    public $date_end;
    public $client_id;
    public $status;
    public $on_created_at;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_start', 'date_end'], 'date'],
            [['client_id', 'status', 'on_created_at'], 'integer']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'date_start' => 'Начало периода',
            'date_end' => 'Конец периода',
            'client_id' => 'Клиент',
            'status' => 'Статус',
            'on_created_at' => 'Фильтровать по',
        ];
    }
}
