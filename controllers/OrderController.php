<?php

namespace app\controllers;

use app\models\Apparatus;
use app\models\ApparatusToClient;
use app\models\constants\UsersConstants;
use app\models\Consumables;
use app\models\OrderToConsumables;
use app\models\Users;
use Yii;
use app\models\Order;
use app\models\search\OrderSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var Users $identity */
        $identity = Yii::$app->user->identity;

        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (UsersConstants::isAdmin()) {
            $dataProvider->query->andWhere(['<>', 'status', Order::STATUS_DRAFT])->andWhere([
                '<>',
                'status',
                Order::STATUS_DONE
            ]);
        } else {
            $dataProvider->query->andWhere(['client_id' => $identity->client_id]);
        }

        Yii::info('1212313', 'test');
        Yii::info(count($dataProvider->getModels()), 'test');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Заказ #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Закрыть',
                    ['class' => 'btn btn-default pull-right', 'data-dismiss' => "modal"])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Order model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the '_from_2' page.
     * @return mixed
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    public function actionCreate()
    {
        /** @var Users $identity */
        $identity = Yii::$app->user->identity;

        $request = Yii::$app->request;
        $model = new Order();
        $model->client_id = $identity->client_id;
        $order_to_cons = new OrderToConsumables();
        $order_to_cons_models = new OrderToConsumables(['order_id' => $model->id]);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Создание заказа.",
                    'size' => 'large',
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                        'order_to_cons' => $order_to_cons,
                        'order_to_cons_models' => $order_to_cons_models,
                    ]),
                    'footer' => Html::button('Закрыть',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить черновик', [
                            'class' => 'btn btn-primary',
                            'type' => "submit",
                            'disabled' => true,
                            'id' => 'submit-order'
                        ])

                ];
            } else {
                if ($model->load($request->post())) {
                    Yii::info($model->attributes, 'test');
                    $model->uploadCountersPhoto();
                    if (!$model->save()) {
                        Yii::error($model->errors, '_error');
                        return [
                            'title' => "Создание заказа.",
                            'size' => 'large',
                            'content' => $this->renderAjax('create', [
                                'model' => $model,
                                'order_to_cons' => $order_to_cons,
                                'order_to_cons_models' => $order_to_cons_models,
                            ]),
                            'footer' => Html::button('Закрыть',
                                    ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Сохранить черновик', [
                                    'class' => 'btn btn-primary',
                                    'type' => 'submit',
                                    'id' => 'submit-order'
                                ])
                        ];
                    }

                    //Добавляем расходники
                    $order_to_cons->load($request->post());
                    $order_to_cons->order_id = $model->id;
                    $order_to_cons->saveConsumables();

                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'forceClose' => true,
                    ];
                } else {
                    return [
                        'title' => "Создание заказа.",
                        'size' => 'large',
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                            'order_to_cons' => $order_to_cons,
                            'order_to_cons_models' => $order_to_cons_models,
                        ]),
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'order_to_cons' => $order_to_cons,
                ]);
            }
        }

    }

    /**
     * Updates an existing Order model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        if (!UsersConstants::isAdmin() && $model->status != $model::STATUS_DRAFT) {
            throw new ForbiddenHttpException('Заявка не может быть отредактирована');
        }

        $order_to_cons_models = $model->orderToConsumables ?? null;
        if (!$order_to_cons_models) {
            $order_to_cons_models = new OrderToConsumables(['order_id' => $model->id]);
        }
        $order_to_cons = new OrderToConsumables();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Редактирование заказа #" . $id,
                    'size' => 'large',
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                        'order_to_cons' => $order_to_cons,
                        'order_to_cons_models' => $order_to_cons_models,
                    ]),
                    'footer' => Html::button('Закрыть',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else {
                if ($model->load($request->post())) {
                    $model->uploadCountersPhoto();

                    if (!$model->save()) {
                        Yii::error($model->errors, '_error');
                        return [
                            'title' => "Редактирование заказа #" . $id,
                            'size' => 'large',
                            'content' => $this->renderAjax('update', [
                                'model' => $model,
                                'order_to_cons' => $order_to_cons,
                                'order_to_cons_models' => $order_to_cons_models,
                            ]),
                            'footer' => Html::button('Закрыть',
                                    ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                        ];
                    }
                    //Добавляем расходники
                    $order_to_cons->load($request->post());
                    $order_to_cons->order_id = $model->id;
                    $order_to_cons->saveConsumables();
                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => "Заказ #" . $id,
                        'content' => $this->renderAjax('view', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Редактировать', ['update', 'id' => $id],
                                ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                    ];
                } else {
                    return [
                        'title' => "Редактирование заказа #" . $id,
                        'size' => 'large',
                        'content' => $this->renderAjax('update', [
                            'model' => $model,
                            'order_to_cons' => $order_to_cons,
                            'order_to_cons_models' => $order_to_cons_models,
                        ]),
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'order_to_cons' => $order_to_cons,
                    'order_to_cons_models' => $order_to_cons_models,
                ]);
            }
        }
    }

    /**
     * Delete an existing Order model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        if (!UsersConstants::isAdmin() && $model->status != $model::STATUS_DRAFT) {
            throw new ForbiddenHttpException('Заявка не может быть удалена');
        }
        $model->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Order model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Присваивает заказу статус "Новый"
     * @param int $id Идентификатор заказа
     * @return array
     * @throws NotFoundHttpException
     * @throws \HttpInvalidParamException
     */
    public function actionToNew($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $order = $this->findModel($id);

        if (!is_file($order->photos)) {
                return [
                    'title' => 'Ошибка отправки заказа',
                    'content' => 'Не загружена фотография счетчиков',
                    'footer' => Html::button('Закрыть',
                        ['class' => 'btn btn-default pull-right', 'data-dismiss' => "modal"])
                ];
        }

        $order->status = $order::STATUS_NEW;

        if (!$order->save()) {
            Yii::error($order->errors, '_error');
        }

        $order->sendNotify();

        return [
            'forceReload' => '#crud-datatable-pjax',
            'title' => 'Отправка заказа',
            'content' => 'Заказ на расходные материалы отправлен успешно!',
            'footer' => Html::button('Закрыть',
                ['class' => 'btn btn-default pull-right', 'data-dismiss' => "modal"])
        ];
    }

    /**
     * Скачивание файла
     * @param string $file Путь к файлу
     * @return \yii\console\Response|Response
     * @throws NotFoundHttpException
     */
    public function actionDownload($file)
    {
        if (is_file($file)) {
            return Yii::$app->response->sendFile($file);
        } else {
            throw new NotFoundHttpException('Запрашиваемый файл не найден');
        }
    }

    /**
     * Удаляет файлы для выбранного счетчика
     * @return array
     */
    public function actionRemoveFiles()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;

        $order_id = $request->post('id');
        $counter = $request->post('counter');

        $order = Order::findOne($order_id);

        if (!$order) {
            return ['success' => 0];
        }
        $dir = $order->photos;

        $files = array_diff(scandir($dir), ['..', '.']);

        foreach ($files as $file_name) {
            $path_file = $dir . '/' . $file_name;

            if (is_file($path_file)) {
                if (explode('_', $file_name)[0] == $counter) {
                    //Если  файл относиться к счетчику (имена фоток счетчика = НомерСчетчика_НомерФоткиПоПорядку.jpg)
                    try {
                        unlink($path_file);
                    } catch (\Exception $e) {
                        Yii::error($e->getMessage(), '_error');
                    }
                }
            }
        }
        return ['success' => 1];
    }

    /**
     * Меняет статусы для заказа
     * @param integer $id Идентификатор заказа
     * @param integer $status Статус
     * @return array|Response
     */
    public function actionChangeStatus($id, $status)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $order = Order::findOne($id) ?? null;

        if (!$order) {
            return [
                'title' => 'Ошибка',
                'content' => 'Заказ не найден'
            ];
        }

        $order->status = $status;

        if ($order->status == $order::STATUS_DONE){
            $order->done_at = date('Y-m-d H:i:s', time());
        }

        if (!$order->save()) {
            Yii::error($order->errors, '_error');
        }

        if ($order->status == $order::STATUS_WORK) {
            return $this->redirect('/order');
        }

        return [
            'forceReload' => '#crud-datatable-pjax',
            'forceClose' => true,
        ];
    }

    public function actionGetConsumablesForApparatus()
    {
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;

        $apparatus = Apparatus::findOne($request->post('id')) ?? null;

        if (!$apparatus) {
            return ['success' => 0, 'error' => 'Ошибка получчения списка расходников. Аппарат не найден'];
        }

        $list = '<option value>Выберите расходник</option>';

        /** @var Consumables $cons_model */
        foreach (Consumables::find()
                     ->andWhere(['apparatus_type' => $apparatus->type])
                     ->each() as $cons_model) {
            $list .= '<option value="' . $cons_model->id . '">' . $cons_model->name . '</option>';
        }

        return ['success' => 1, 'data' => $list];


    }

    /**
     * Получаает тип аппарата и список расходников для данного типа аппарата
     * @param integer $id Идентификатор аппарата клиента
     * @return array
     */
    public function actionGetInfo($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $atc = ApparatusToClient::findOne($id);

        $apparatus = Apparatus::findOne($atc->apparatus_id) ?? null;

        if (!$apparatus) {
            return ['success' => 0, 'error' => 'Ошибка получчения списка расходников. Аппарат не найден'];
        }

        $list = '<option value>Выберите расходник</option>';

        /** @var Consumables $cons_model */
        foreach (Consumables::find()
                     ->andWhere(['apparatus_type' => $apparatus->type])
                     ->each() as $cons_model) {
            $list .= '<option value="' . $cons_model->id . '">' . $cons_model->name . '</option>';
        }

        return ['success' => 1, 'data' => $list, 'type' => $apparatus->type];

    }
}
