<?php

namespace app\models\constants;

use app\models\Users;
use Yii;
use yii\helpers\Url;


class UsersConstants extends Users
{
    const ROLE_ADMIN = 1;
    const ROLE_CLIENT = 2;


    public static function getString($role)
    {
        switch ($role) {
            case self::ROLE_ADMIN:
                return 'Администратор';
            case self::ROLE_CLIENT:
                return 'Клиент';
        }
        return 'Неизвестный';
    }

    public static function getArray()
    {
        return [
            self::ROLE_ADMIN => self::getString(self::ROLE_ADMIN),
            self::ROLE_CLIENT => self::getString(self::ROLE_CLIENT),
        ];
    }

    public static function getImage($image)
    {
        if (!file_exists($image) || $image == '') {
//            $path = 'http://' . $_SERVER['SERVER_NAME'].'/img/nouser.png';
            $path = Url::to(['@web/img/nouser.png']);
        } else {
//            $path = 'http://' . $_SERVER['SERVER_NAME'] . '/uploads/avatars/' . $image;
            $path = Url::to(['@web/' . $image]);

        }
        return $path;
    }

    /**
     * Проверяет пользователя на админа
     * @return bool
     */
    public  static function isAdmin()
    {
        /** @var Users $identity */
        $identity = Yii::$app->user->identity;
        if ($identity){
            return $identity->role === self::ROLE_ADMIN;
        }

        return false;
    }

    public static function isSuperAdmin ()
    {
        /** @var Users $identity */
        $identity = Yii::$app->user->identity;

        return $identity->id === 1;
    }

}
