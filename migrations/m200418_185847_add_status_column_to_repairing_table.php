<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%repairing}}`.
 */
class m200418_185847_add_status_column_to_repairing_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%repairing}}', 'status', $this->smallInteger()->defaultValue(1)->comment('Статус'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%repairing}}', 'status');
    }
}
