<?php

namespace app\models\query;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\app\models\ApparatusToClient]].
 *
 * @see \app\models\ApparatusToClient
 */
class ApparatusToClientQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    public function forClient($id)
    {
        return $this->andWhere(['client_id' => $id]);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\ApparatusToClient[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\ApparatusToClient|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
