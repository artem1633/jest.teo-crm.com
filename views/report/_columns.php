<?php

use app\models\constants\UsersConstants;
use app\models\Report;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
        'visible' => UsersConstants::isAdmin(),
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'created_at',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'report_month',
        'filter' => (new Report())->getMonthList(),
        'content' => function (Report $model) {
            return $model->getMonthName($model->report_month) .', ' . $model->report_year;
        },
        'hAlign' => 'center',
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'client_name',
        'content' => function (Report $model) {
            return $model->apparatusToClient->client ? $model->apparatusToClient->client->official_name : null;
        },
        'visible' => UsersConstants::isAdmin(),
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'apparatus_full_name',
        'content' => function (Report $model) {
            $apparatus = $model->apparatus->getApparatusFullName() . ' (№&nbsp;' . $model->apparatusToClient->serial_number . ')';
            return $apparatus;
        },
        'hAlign' => 'center',
        'vAlign' => 'middle',
    ],

    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'counter_1',
        'content' => function (Report $model) {
            if ($model->apparatus) {
                if ($model->apparatus->type == 1) {
                    return $model->counter_1;
                } else {
                    return $model->counter_1 . '&nbsp(черные)' . '<br>'
                        . $model->counter_2 . '&nbsp(цветные)';
                }
            }
            return null;
        },
        'label' => 'Показания счетчика',
        'hAlign' => 'center',
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'photo',
        'content' => function (Report $model) {
            return $model->getCountersPhoto();
        },
        'hAlign' => 'center',
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'status',
        'filter' => (new Report())->getStatusesList(),
        'content' => function (Report $model) {
            $status_name = $model->getStatusName($model->status);

            $btn = $model->getButton();

            return '<p class="margin-bottom: 0.5rem;"><b>' . $status_name . '</b></p>' . $btn;
        },
        'format' => 'raw',
        'hAlign' => 'center',
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'sented_at',
        'format' => 'datetime',
        'hAlign' => 'center',
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'content' => function (Report $model) {
            if ($model->status == $model::STATUS_NEW) {
                return Html::a('Подтвердить',
                    ['change-status', 'id' => $model->id, 'status' => $model::STATUS_APPROVED], [
                        'role' => 'modal-remote',
                        'title' => 'Подтвердить показания',
                        'data-confirm' => false,
                        'data-method' => false,// for overide yii data api
                        'data-request-method' => 'post',
                        'data-toggle' => 'tooltip',
                        'data-confirm-title' => 'Подтверждение показаний счетчика',
                        'data-confirm-message' => 'Подтверждаете показания?',
                        'data-confirm-ok' => 'Подтверждаю',
                        'data-confirm-cancel' => 'Отмена',
                        'class' => 'btn btn-info'
                    ]);
            }
            return '';
        },
        'visible' => UsersConstants::isAdmin(),
        'vAlign' => 'middle',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
//        'contentOptions' => [
//            'style' => 'font-size: 1.5rem;',
//        ],
        'dropdown' => false,
        'vAlign' => 'middle',
        'template' => '{reject} {view} {edit} {delete}',
        'urlCreator' => function ($action, $model, $key) {
            return Url::to([$action, 'id' => $key]);
        },
        'buttons' => [
            'update' => function ($url, Report $model) {
                if ($model->status === $model::STATUS_DRAFT || UsersConstants::isAdmin()) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                        'role' => 'modal-remote',
                        'title' => 'Редактировать',
                        'data-toggle' => 'tooltip',
                    ]);
                } else {
                    return null;
                }
            },
            'delete' => function ($url, Report $model) {
                if ($model->status === $model::STATUS_DRAFT || UsersConstants::isSuperAdmin()) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                        'role' => 'modal-remote',
                        'title' => 'Удалить заявку',
                        'data-confirm' => false,
                        'data-method' => false,// for overide yii data api
                        'data-request-method' => 'post',
//                        'data-toggle' => 'tooltip',
                        'data-confirm-title' => 'Вы уверены?',
                        'data-confirm-message' => 'Подтвердите удаление заказа',
                        'data-confirm-ok' => 'Удалить',
                        'data-confirm-cancel' => 'Отмена',
                    ]);
                } else {
                    return null;
                }
            },
//            'reject' => function ($url, Report $model) {
//                if (UsersConstants::isAdmin()) {
//                    return Html::a('<span class="glyphicon glyphicon-thumbs-down"></span>',
//                        ['reject', 'id' => $model->id], [
//                            'role' => 'modal-remote',
//                            'title' => 'Отклонить отчет',
//                            'data-confirm' => false,
//                            'data-method' => false,// for overide yii data api
//                            'data-request-method' => 'post',
////                        'data-toggle' => 'tooltip',
//                            'data-confirm-title' => 'Отклонение отчета',
//                            'data-confirm-message' => 'Действительно отклонить отчет?',
//                            'data-confirm-ok' => 'Отклонить',
//                            'data-confirm-cancel' => 'Отмена',
//                        ]);
//                } else {
//                    return null;
//                }
//            }
        ],
        'viewOptions' => [
            'role' => 'modal-remote',
            'title' => 'Просмотр',
            'data-toggle' => 'tooltip',
            'class' => 'view-repairing'
        ],
    ],

];   