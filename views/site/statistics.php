<?php


use app\models\Client;
use app\models\Order;
use app\models\Repairing;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use kartik\select2\Select2;
use kartik\switchinput\SwitchInput;
use yii\base\InvalidConfigException;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var array $stat_info Статистическая информация */
/* @var $this yii\web\View */
/* @var $statistics_model \app\models\Statistics */
/* @var $orderDataProvider \yii\data\ActiveDataProvider */
/* @var $repairingDataProvider \yii\data\ActiveDataProvider */
/* @var $numCopiesDataProvider \yii\data\ActiveDataProvider */

$this->title = 'Джест';
CrudAsset::register($this);

?>

    <div class="row info-block">
        <div class="col-md-4 col-sm-6">
            <div class="widget widget-stats bg-green">
                <div class="stats-icon"><i class="fa fa-cubes"></i></div>
                <div class="stats-info">
                    <h4>ИСПОЛНЕНО ЗАКАЗОВ НА РМ</h4>
                    <p><?= $stat_info['order_count_done'] ?></p>
                </div>
                <div class="stats-link">
                    <?= Html::a('Детали <i class="fa fa-arrow-circle-o-right"></i>', [
                        '/order',
                        'OrderSearch[status]' => Order::STATUS_DONE
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class="widget widget-stats bg-blue">
                <div class="stats-icon"><i class="fa fa-chain-broken"></i></div>
                <div class="stats-info">
                    <h4>ЗАКРЫТО ИНЦИДЕНТОВ</h4>
                    <p><?= $stat_info['repairing_count_done']; ?></p>
                </div>
                <div class="stats-link">
                    <?= Html::a('Детали <i class="fa fa-arrow-circle-o-right"></i>', [
                        '/repairing',
                        'RepairingSearch[status]' => Repairing::STATUS_DONE
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class="widget widget-stats bg-purple">
                <div class="stats-icon"><i class="fa fa-television"></i></div>
                <div class="stats-info">
                    <h4>ЗАКАЗОВ И ИНЦИДЕНТОВ ВСЕГО</h4>
                    <p><?= (int)$stat_info['order_count_all'] + (int)$stat_info['repairing_count_all'] ?></p>
                </div>
                <div class="stats-link">
                    <?= Html::a('Детали <i class="fa fa-arrow-circle-o-right"></i>', [
                        '/site/statistics'
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

<?php $form = ActiveForm::begin() ?>
    <div class="panel panel-inverse filter-block">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                            class="fa fa-expand"></i></a>
                <a href="javascript:" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i
                            class="fa fa-minus"></i></a>
            </div>
            <h4 class="panel-title">Фильтры</h4>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-3">
                    <?php
                    try {
                        echo $form->field($statistics_model, 'date_start')->input('date');
                    } catch (InvalidConfigException $e) {
                        Yii::error($e->getMessage(), '_error');
                        echo $e->getMessage();
                    }
                    ?>
                </div>
                <div class="col-md-3">
                    <?php
                    try {
                        echo $form->field($statistics_model, 'date_end')->input('date');
                    } catch (InvalidConfigException $e) {
                        Yii::error($e->getMessage(), '_error');
                        echo $e->getMessage();
                    }
                    ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($statistics_model, 'on_created_at')->widget(SwitchInput::class, [
                        'pluginOptions' => [
                            'onText' => 'Дате создания',
                            'offText' => 'Дате выполнения'
                        ]
                    ]) ?>
                </div>
                <div class="col-md-2">

                    <?= $form->field($statistics_model, 'status')->widget(Select2::class, [
                        'data' => (new Order())->getStatusList(),
                        'options' => [
                            'placeholder' => 'Статус',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                        'hideSearch' => true,
                    ]) ?>
                </div>
            </div>
            <div class="row" style="height: 7rem;">
                <div class="col-md-7">
                    <?= $form->field($statistics_model, 'client_id')->widget(Select2::class, [
                        'data' => (new Client())->getList(),
                        'options' => [
                            'placeholder' => 'Выбреите клиента'
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ]) ?>
                </div>
                <div class="col-md-5"
                     style="display: flex;align-items: center;height: inherit;justify-content: flex-end;">
                    <div class="btn-group">
                        <?= Html::a('Сбросить фильтры', ['/site/statistics'], [
                            'class' => 'btn btn-warning',
                            'id' => 'filter-reset-btn'
                        ]) ?>
                        <?= Html::submitButton('Применить фильтры', [
                            'class' => 'btn btn-success',
                        ]) ?>
                    </div>

                </div>
            </div>

        </div>
    </div>
<?php ActiveForm::end(); ?>

    <!--Заказы расходных материалов-->
    <div class="panel panel-inverse order-index">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                            class="fa fa-expand"></i></a>
                <a href="javascript:" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i
                            class="fa fa-minus"></i></a>
            </div>
            <h4 class="panel-title">Заказы расходных материалов</h4>
        </div>
        <div class="panel-body" style="display:none">
            <div id="ajaxCrudDatatable">
                <?php
                try {
                    echo GridView::widget([
                        'id' => 'crud-datatable',
                        'dataProvider' => $orderDataProvider,
//                    'filterModel' => $orderSsearchModel,
                        'tableOptions' => ['class' => 'table table-bordered'],
                        'pjax' => true,
                        'columns' => require(Url::to('@app/views/order/_columns.php')),
                        'responsiveWrap' => false,
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
//                    'panel' => [
//                        'headingOptions' => ['style' => 'display: none;'],
//                        '<div class="clearfix"></div>',
//                    ]
                    ]);
                } catch (Exception $e) {
                    Yii::error($e->getMessage(), '_error');
                    echo $e->getMessage();
                } ?>
            </div>
        </div>
    </div>
    <!--Заявки на ремонт-->
    <div class="panel panel-inverse repairing-index">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                            class="fa fa-expand"></i></a>
                <a href="javascript:" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i
                            class="fa fa-minus"></i></a>
            </div>
            <h4 class="panel-title">Заявки на ремонт</h4>
        </div>
        <div class="panel-body" style="display:none">
            <div id="ajax-crud-datatable-repair">
                <?php
                try {
                    echo GridView::widget([
                        'id' => 'crud-datatable-repairs',
                        'dataProvider' => $repairingDataProvider,
//                    'filterModel' => $repairingSearchModel,
                        'tableOptions' => ['class' => 'table table-bordered'],
                        'pjax' => true,
                        'columns' => require(Url::to('@app/views/repairing/_columns.php')),
                        'responsiveWrap' => false,
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
//                    'panel' => [
//                        'headingOptions' => ['style' => 'display: none;'],
//                        '<div class="clearfix"></div>',
//                    ]
                    ]);
                } catch (Exception $e) {
                    Yii::error($e->getMessage(), '_error');
                    echo $e->getMessage();
                } ?>
            </div>
        </div>
    </div>
    <!--Количество копий-->
    <div class="panel panel-inverse num-copies-index">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                            class="fa fa-expand"></i></a>
                <a href="javascript:" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i
                            class="fa fa-minus"></i></a>
            </div>
            <h4 class="panel-title">Количество копий</h4>
        </div>
        <div class="panel-body" style="display:none">
            <div id="ajax-crud-datatable-num-copies">
                <?php
                try {
                    echo GridView::widget([
                        'id' => 'crud-datatable-number',
                        'dataProvider' => $numCopiesDataProvider,
                        'tableOptions' => ['class' => 'table table-bordered'],
                        'pjax' => true,
                        'columns' => require(Url::to('@app/views/report/_num_columns.php')),
                        'responsiveWrap' => false,
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
//                    'panel' => [
//                        'headingOptions' => ['style' => 'display: none;'],
//                        '<div class="clearfix"></div>',
//                    ]
                    ]);
                } catch (Exception $e) {
                    Yii::error($e->getMessage(), '_error');
                    echo $e->getMessage();
                } ?>
            </div>
        </div>
    </div>


<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>