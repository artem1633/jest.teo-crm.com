<?php

use app\models\constants\UsersConstants;
use johnitvn\ajaxcrud\CrudAsset;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $settings app\models\Settings[] */

$this->title = 'Настройки';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="panel panel-inverse settings-index">
    <div class="panel-heading">
        <h4 class="panel-title">Settings</h4>
    </div>
    <?php if (count($settings) > 0) { ?>
    <?php $form = ActiveForm::begin([
        'options' => [
            'class' => 'form-horizontal form-bordered',
            'style' => 'padding: 0;',
        ]
    ]); ?>

    <div class="panel-body">
        <?php foreach ($settings as $setting): ?>
            <div class="row">
                <div class="form-group">
                    <label for=""
                           class="control-label col-md-4"><?= Yii::$app->formatter->asRaw($setting->label) ?></label>
                    <div class="col-md-8">
                        <input name="Settings[<?= $setting->key ?>]" type="text"
                               value="<?= $setting->value ?>"
                               class="form-control" <?= UsersConstants::isSuperAdmin() ? '' : 'disabled'; ?>>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>

        <div class="row">
            <div class="col-md-12">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
                       value="<?= Yii::$app->request->csrfToken ?>">
                <?php if (UsersConstants::isSuperAdmin()): ?>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group pull-right">
                            <?= Html::submitButton('Сохранить', [
                                'class' => 'btn btn-success btn-block',
                            ]) ?>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
                <?php ActiveForm::end(); ?>

                <?php } else { ?>
                    <div class="note note-danger" style="margin: 15px;">
                        <h4><i class="fa fa-exclamation-triangle"></i> Настройки не найдены!
                        </h4>
                        <p>
                            Убедитесь, что все миграции выполнились без ошибок
                        </p>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
