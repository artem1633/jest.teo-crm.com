<?php

use yii\db\Migration;

/**
 * Class m200603_112342_change_relation_to_repairing_table
 */
class m200603_112342_del_relation_to_repairing_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk-repairing-apparatus_id', '{{%repairing}}');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200603_112342_change_relation_to_repairing_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200603_112342_change_relation_to_repairing_table cannot be reverted.\n";

        return false;
    }
    */
}
