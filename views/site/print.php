<table style="width:100%; border: 0; border-collapse: collapse; text-align: left;" border="1" >
    <tr >
        <td colspan="9" style=" text-align: left; font-size: 20px; padding-left: 10px;">ЛДСП</td>
    </tr>
    <thead>
        <tr>
            <th  style=" text-align: center;padding:3px;">№ детали</th>
            <th  style=" text-align: center;padding:3px;">Цвет</th>
            <th  style=" text-align: center;padding:3px;">Толщина</th>
            <th  style=" text-align: center;padding:3px;">Длина</th>
            <th  style=" text-align: center;padding:3px;">Ширина</th>
            <th  style=" text-align: center;padding:3px;">Количество</th>
            <th  style=" text-align: center;padding:3px;">Кромка по длине</th>
            <th  style=" text-align: center;padding:3px;">Кромка по ширине</th>
            <th  style=" text-align: center;padding:3px;">Примечание</th>


        </tr>
    </thead>
    <tbody>
        <tr>
            <td style=" text-align: left;padding:3px;">1</td>
            <td style=" text-align: left;padding:3px;">Материал заказчика</td>
            <td style=" text-align: left;padding:3px;">16</td>
            <td style=" text-align: left;padding:3px;">300</td>
            <td style=" text-align: left;padding:3px;">300</td>
            <td style=" text-align: left;padding:3px;">2</td>
            <td style=" text-align: left;padding:3px;">Нет - Нет</td>
            <td style=" text-align: left;padding:3px;">Нет - Нет</td>
            <td style=" text-align: left;padding:3px;">-</td>
        </tr>
    </tbody>
</table>
<br>
<br>
<table style="width:100%; border: 0; border-collapse: collapse; text-align: left;" border="1" >
    <tr >
        <td colspan="6" style=" text-align: left; font-size: 20px; padding-left: 10px;">Оргалит</td>
    </tr>
    <thead>
        <tr>
            <th  style=" text-align: center;padding:3px;">№ детали</th>
            <th  style=" text-align: center;padding:3px;">Цвет</th>
            <th  style=" text-align: center;padding:3px;">Длина</th>
            <th  style=" text-align: center;padding:3px;">Ширина</th>
            <th  style=" text-align: center;padding:3px;">Количество</th>
            <th  style=" text-align: center;padding:3px;">Примечание</th>


        </tr>
    </thead>
    <tbody>
        <tr>
            <td style=" text-align: left;padding:3px;">1</td>
            <td style=" text-align: left;padding:3px;">Материал заказчика</td>
            <td style=" text-align: left;padding:3px;">300</td>
            <td style=" text-align: left;padding:3px;">300</td>
            <td style=" text-align: left;padding:3px;">2</td>
            <td style=" text-align: left;padding:3px;">-</td>
        </tr>
    </tbody>
</table>

<br>
<br>
<table style="width:100%; border: 0; border-collapse: collapse; text-align: left;" border="1" >
    <tr >
        <td colspan="3" style=" text-align: left; font-size: 20px; padding-left: 10px;">Эскизы (общее количество)</td>
    </tr>
    <thead>
        <tr>
            <th  style=" text-align: center;padding:3px;">Радиус 50-400 мм</th>
            <th  style=" text-align: center;padding:3px;">Радиус 450-1000 мм</th>
            <th  style=" text-align: center;padding:3px;">Прямоугольный выпил</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style=" text-align: left;padding:3px;">3</td>
            <td style=" text-align: left;padding:3px;">1</td>
            <td style=" text-align: left;padding:3px;">1</td>
        </tr>
    </tbody>
</table>

<br>
<br>
<table style="width:100%; border: 0; border-collapse: collapse; text-align: left;" border="1" >
    <tr >
        <td colspan="4" style=" text-align: left; font-size: 20px; padding-left: 10px;">Фрезеровка (общее количество)</td>
    </tr>
    <thead>
        <tr>
            <th  style=" text-align: center;padding:3px;">Паз (м.п.)</th>
            <th  style=" text-align: center;padding:3px;">Четверть (м.п.)</th>
            <th  style=" text-align: center;padding:3px;">Наклонный пил (м.п.)</th>
            <th  style=" text-align: center;padding:3px;">Криволинейный пил (шт.)</th>

        </tr>
    </thead>
    <tbody>
        <tr>
            <td style=" text-align: left;padding:3px;">1</td>
            <td style=" text-align: left;padding:3px;">2</td>
            <td style=" text-align: left;padding:3px;">1</td>
            <td style=" text-align: left;padding:3px;">1</td>
        </tr>
    </tbody>
</table>

<br>
<br>
<table style="width:100%; border: 0; border-collapse: collapse; text-align: left;" border="1" >
    <tr >
        <td colspan="3" style=" text-align: left; font-size: 20px; padding-left: 10px;">Присадка (общее количество)</td>
    </tr>
    <thead>
        <tr>
            <th  style=" text-align: center; padding:3px;">1 - 10 мм</th>
            <th  style=" text-align: center; padding:3px;">11 - 20 мм</th>
            <th  style=" text-align: center; padding:3px;">21 - 35 мм</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style=" text-align: left; padding:3px;">1</td>
            <td style=" text-align: left; padding:3px;">0</td>
            <td style=" text-align: left; padding:3px;">1</td>
        </tr>
    </tbody>
</table>