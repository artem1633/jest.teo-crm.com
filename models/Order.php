<?php

namespace app\models;

use app\components\EmailHelper;
use app\models\constants\UsersConstants;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\UploadedFile;


/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property string $created_at
 * @property int $client_id Клиент
 * @property int $apparatus_id Аппарат
 * @property int $counter_1 Счетчик 1
 * @property int $counter_2 Счетчик 2
 * @property int $counter_3 Счетчик 3
 * @property string $photos Путь к папке с фото счетчиков
 * @property string $done_at Дата закрытия заявки
 * @property int $status Статус заявки
 *
 * @property Apparatus $apparatus
 * @property ApparatusToClient $apparatusToClient
 * @property Client $client
 * @property OrderToConsumables[] $orderToConsumables
 * @property Consumables[] $consumables
 */
class Order extends ActiveRecord
{
    const STATUS_DRAFT = 1;
    const STATUS_NEW = 2;
    const STATUS_WORK = 3;
    const STATUS_DONE = 4;

    const COUNTER_TYPE_GENERAL = 1;
    const COUNTER_TYPE_BLACK = 2;
    const COUNTER_TYPE_COLORED = 3;

    const SCENARIO_COLORED = 'colored';
    const SCENARIO_MONOCHROME = 'monochrome';


    public $consumables;
    public $consumable_info;
    public $imageFile;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'done_at', 'consumable_info'], 'safe'],
            [['client_id', 'apparatus_id', 'counter_1', 'counter_2', 'counter_3', 'status'], 'integer'],
            [['photos'], 'string'],
            [
                ['apparatus_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => ApparatusToClient::className(),
                'targetAttribute' => ['apparatus_id' => 'id']
            ],
            [
                ['client_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Client::className(),
                'targetAttribute' => ['client_id' => 'id']
            ],
            [['apparatus_id'], 'required', 'message' => 'Необходимо выбрать аппарат'],
            [['counter_1', 'counter_2', 'counter_3'], 'required', 'on' => self::SCENARIO_COLORED],
            [['counter_1'], 'required', 'on' => self::SCENARIO_MONOCHROME],
            [['apparatus_id', 'photos'], 'required'],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpeg, jpg, png, gif'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Дата создания',
            'client_id' => 'Клиент',
            'apparatus_id' => 'Аппарат',
            'counter_1' => 'Счетчик Общий',
            'counter_2' => 'Счетчик Черный',
            'counter_3' => 'Счетчик Цветной',
            'photos' => 'Фото счетчиков',
            'done_at' => 'Дата закрытия',
            'status' => 'Статус',
            'consumables' => 'Заказано',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApparatusToClient()
    {
        return $this->hasOne(ApparatusToClient::className(), ['id' => 'apparatus_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApparatus()
    {
        return $this->hasOne(Apparatus::className(), ['id' => 'apparatus_id'])
            ->via('apparatusToClient');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getConsumables()
    {
        return $this->hasMany(Consumables::className(), ['id' => 'consumables_id'])
            ->viaTable(OrderToConsumables::tableName(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderToConsumables()
    {
        return $this->hasMany(OrderToConsumables::className(), ['order_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\OrderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\OrderQuery(get_called_class());
    }

    /**
     * Массив статусов и наименований
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_DRAFT => 'Черновик',
            self::STATUS_NEW => 'Новый',
            self::STATUS_WORK => 'В работе',
            self::STATUS_DONE => 'Выполнено',
        ];
    }

    /**
     * Возвращает наименование статуса по ID
     * @param int $status Статус
     * @return string
     */
    public function getStatusName($status)
    {
        return $this->getStatusList()[$status];
    }

    /**
     * Загрузка фоток счетчиков
     * @return bool
     * @throws \yii\base\Exception
     */
    public function uploadCountersPhoto()
    {
        $form = new UploadForm();
        $form->scenario = $form::SCENARIO_IMAGE;

        $target_dir = 'uploads/counters/order/' . $this->client_id;

        //=======================//
        $target_name = Yii::$app->security->generateRandomString(10);

        Yii::info('Целевая папка: ' . $target_dir, 'test');

        $form->file = UploadedFile::getInstance($this, 'imageFile');
        Yii::info($form->file, 'test');

        if ($form->file) {
            Yii::info('Имя файла: ' . $target_name, 'test');
            $path_file = $form->upload($target_dir, $target_name);
        } else {
            Yii::info('Файл не загружен', 'test');
            return false;
        }
        //============//

//        $ext = pathinfo($_FILES['Order']['name']['image'][0], PATHINFO_EXTENSION);
//        $target_name = Yii::$app->security->generateRandomString(10) . '.' . $ext;
//        $path_file = $target_dir . '/' . $target_name;
//        move_uploaded_file($_FILES['Order']['tmp_name']['image'][0], $path_file);

        Yii::info($path_file, 'test');

        $this->photos = $path_file;

        return true;
    }

    /**
     * Возвращает список ссылок на фотки счетчиков (для отображния например в карточке заказа)
     * @param string $separator Разделитель строк
     * @param int $target_counter_num Номер счетчика (0, 1, 2 или 3) Ноль - возвращать все файлы
     * @return string|array
     */
    public function getListOfPhoto($separator = PHP_EOL, $target_counter_num = 0)
    {
        if (!is_dir($this->photos)) {
            return false;
        }
        $dir = $this->photos;

        $files = array_diff(scandir($dir), ['..', '.', '.gitignore']);

        $prepared_photos = [];

        foreach ($files as $file_name) {
            $path_file = $dir . '/' . $file_name;
            if (is_file($path_file)) {
                if ($target_counter_num == 0) {
                    $prepared_photos[] = Html::a($file_name, ['download', 'file' => $path_file]) . $separator;
                } else {
                    $counter_num = explode('_', $file_name)[0];
                    $prepared_photos[$counter_num][] = Html::a($file_name,
                            ['download', 'file' => $path_file]) . $separator;
                }
            } else {
                Yii::warning('Пропущено. Файл: ' . $path_file, 'test');
                continue;
            }
        }

        Yii::info($prepared_photos, 'test');

        if ($target_counter_num == 0) {
            return implode('', $prepared_photos);
        } else {
            return implode('', $prepared_photos[$target_counter_num]);
        }
    }

    /**
     * Отправка почты о новом заказе админу
     * @return void
     * @throws \HttpInvalidParamException
     */
    public function sendNotify()
    {
        $url = Url::to(['/order/view', 'id' => $this->id], true);

        $email = new EmailHelper();
        $email->email_type = EmailHelper::TYPE_ORDER;
        $email->subject = $this->client->official_name . ', ' . $this->apparatus->getApparatusFullName() . ', расходники!';

        $email->html_body = '<br><p>Для перехода в заявку нажмите ' . Html::a('здесь',
                $url) . '. Или скопируйте в адресную строку ссылку: ' . $url . ' </p>';

        $email->send();
    }

    /**
     * Список счетчиков
     * @return array
     */
    public function getCounterList()
    {
        return [
            self::COUNTER_TYPE_GENERAL => 'Общий',
            self::COUNTER_TYPE_BLACK => 'Черный',
            self::COUNTER_TYPE_COLORED => 'Цветной',
        ];
    }

    /**
     * Вохвращает наименование счетчика
     * @param integer $counter Счетчик (1,2,3)
     * @return mixed
     */
    public function getCounterName($counter)
    {
        return $this->getCounterList()[$counter];
    }

    /**
     * Получает для заказа расходники и кол-во
     * @param bool $as_array
     * @param string $delimiter Разделитель значений
     * @return array|string
     */
    public function getConsumablesList($as_array = false, $delimiter = '')
    {
        $sql = <<<SQL
            SELECT CONCAT(c.name, '&nbsp;(', otc.number, ')') AS consumable_info FROM order_to_consumables otc
            LEFT JOIN consumables c ON otc.consumables_id = c.id
            WHERE otc.order_id = $this->id
SQL;

        $consumables = OrderToConsumables::findBySql($sql)->column();

        Yii::info($consumables, 'test');

        if ($as_array) {
            return $consumables;
        } else {
            $str = implode($delimiter, $consumables);
            $str = str_replace(' ', '&nbsp',
                $str); //Чтобы в талице не переносились слова для одного наименования расходника
            return $str;
        }
    }

    /**
     * Возвращает ярлычок с количеством новых заявок
     * @return string
     */
    public function getLabelCounts()
    {
        if (!UsersConstants::isAdmin()) {
            return '';
        }

        $new_orders = self::find()->andWhere([
            'status' => self::STATUS_NEW
        ])->count();

        if ($new_orders > 0) {
            return "<span class='label label-warning' style='margin-left: 5px;' title='Новые заказы'>{$new_orders}</span>";
        }

        return '';
    }

    /**
     * Возвращает кнопку для смены статуса заявки на РМ
     * @return null|string
     */
    public function getButton()
    {
        $status = $this->status + 1;

        if ($status > self::STATUS_DONE) {
            return null;
        }

        if ($status == self::STATUS_WORK) {
            return Html::a('В работу', ['/order/change-status', 'id' => $this->id, 'status' => $status], [
                'class' => 'btn btn-info btn-sm',
                'title' => 'Заказ в работу',
                'data-toggle' => 'tooltip',
                'role' => 'modal-remote',
                'data-confirm-title' => 'Вы уверены?',
                'data-confirm-message' => 'Сменить статус заказа',
                'data-confirm-ok' => 'В работу',
                'data-confirm-cancel' => 'Отмена',
            ]);
        } elseif ($status == self::STATUS_DONE) {
            return Html::a('Выполнено', ['/order/change-status', 'id' => $this->id, 'status' => $status], [
                'class' => 'btn btn-info btn-sm',
                'title' => 'Завершить заказ',
                'data-toggle' => 'tooltip',
                'role' => 'modal-remote',
                'data-confirm-title' => 'Вы уверены?',
                'data-confirm-message' => 'Завершить заказ',
                'data-confirm-ok' => 'Завершить',
                'data-confirm-cancel' => 'Отмена',
            ]);
        }
        return null;
    }

    /**
     * @return string
     */
    public function getCountersPhoto()
    {
        $photo_path = $this->photos;
        Yii::info(pathinfo($this->photos), 'test');
        $photo_name = pathinfo($this->photos)['filename'];

        return <<<HTML
    <a data-lightbox="{$photo_name}" href="/{$photo_path}">
        <img style="max-width: 100px" src="/{$photo_path}" alt="{$photo_name}">
    </a>
HTML;
    }
}
