<?php

use yii\db\Migration;

/**
 * Class m200603_112558_change_relation_to_order_table
 */
class m200603_112558_del_relation_to_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk-order-apparatus_id', '{{%order}}');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200603_112558_change_relation_to_order_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200603_112558_change_relation_to_order_table cannot be reverted.\n";

        return false;
    }
    */
}
