<?php

use app\models\ApparatusToClient;
use app\models\Repairing;
use yii\db\Migration;

/**
 * Class m200603_113120_correct_repairings_table
 */
class m200603_113120_correct_repairings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $repairs = Repairing::find()->all();
        Yii::info('Repairs count: ' . count($repairs), 'test');

        /** @var Repairing $repair */
        foreach ($repairs as $repair) {
            //Ищем аппарат клиента
            $atc = ApparatusToClient::find()->andWhere([
                'apparatus_id' => $repair->apparatus_id,
                'client_id' => $repair->client_id
            ])->one();

            if ($atc){
                //меняем ID аппарата на ID аппарата клиента
                $repair->apparatus_id = $atc->id;
                $repair->save(false);
            }
        }

        $this->addForeignKey(
            'fk-repairing-apparatus_to_client_id',
            '{{%repairing}}',
            'apparatus_id',
            '{{%apparatus_to_client}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200603_113120_correct_repairings_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200603_113120_correct_repairings_table cannot be reverted.\n";

        return false;
    }
    */
}
