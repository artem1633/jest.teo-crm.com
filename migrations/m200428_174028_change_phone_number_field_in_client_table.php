<?php

use yii\db\Migration;

/**
 * Class m200428_174028_change_phone_number_field_in_client_table
 */
class m200428_174028_change_phone_number_field_in_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%client}}', 'phone_number', $this->string()->comment('Телефон'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200428_174028_change_phone_number_field_in_client_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200428_174028_change_phone_number_field_in_client_table cannot be reverted.\n";

        return false;
    }
    */
}
