<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Repairing;

/**
 * RepairingSearch represents the model behind the search form about `app\models\Repairing`.
 */
class RepairingSearch extends Repairing
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'client_id', 'apparatus_id', 'counter_1', 'counter_2', 'counter_3', 'status'], 'integer'],
            [['created_at', 'description', 'photos', 'done_at'], 'safe'],
            [['client_name'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Repairing::find();
        $query->joinWith('client');
        $query->orderBy(['status' => SORT_ASC, 'created_at' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'created_at',
                    'client_name',
                    'apparatus_id',
                    'counter_1',
                    'done_at',
                    'status'
                ],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'client_id' => $this->client_id,
            'apparatus_id' => $this->apparatus_id,
            'counter_1' => $this->counter_1,
            'counter_2' => $this->counter_2,
            'counter_3' => $this->counter_3,
            'done_at' => $this->done_at,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'photos', $this->photos])
            ->andFilterWhere(['like', 'client.official_name', $this->client_name]);

        return $dataProvider;
    }
}
