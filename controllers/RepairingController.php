<?php

namespace app\controllers;

use app\models\Apparatus;
use app\models\constants\UsersConstants;
use app\models\Users;
use Yii;
use app\models\Repairing;
use app\models\search\RepairingSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * RepairingController implements the CRUD actions for Repairing model.
 */
class RepairingController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Repairing models.
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var Users $identity */
        $identity =  Yii::$app->user->identity;

        $searchModel = new RepairingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (UsersConstants::isAdmin()) {
            $dataProvider->query->andWhere(['<>', 'status', Repairing::STATUS_DRAFT])->andWhere(['<>', 'status', Repairing::STATUS_DONE]);

        } else {
            $dataProvider->query->andWhere(['client_id' =>$identity->client_id]);
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Repairing model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Заявка #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-right', 'data-dismiss' => "modal"])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Repairing model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    public function actionCreate()
    {
        /** @var Users $identity */
        $identity =  Yii::$app->user->identity;

        $request = Yii::$app->request;
        $model = new Repairing();
        $model->client_id = $identity->client_id;


        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Добавление заявки",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post())) {
                    $apparatus_model = Apparatus::findOne($model->apparatus_id) ?? null;
                    if ($apparatus_model && $apparatus_model->type == Apparatus::TYPE_COLORED){
                        $model->scenario = $model::SCENARIO_COLORED;
                    } else {
                        $model->scenario = $model::SCENARIO_MONOCHROME;
                    }
                   if (! $model->save()){
                        Yii::error($model->errors, '_error');
                       return [
                           'title' => "Добавление заявки",
                           'content' => $this->renderAjax('create', [
                               'model' => $model,
                           ]),
                           'footer' => Html::button('Закрыть',
                                   ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                               Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                       ];
                   }
                    $model->uploadPrintPhoto();

                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'forceClose' => true,
                    ];
                } else {
                    return [
                        'title' => "Добавление заявки",
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing Repairing model.
     * For ajax request will close form and update table
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Редактирование заявки #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else {
                if ($model->load($request->post())) {
                    $apparatus_model = Apparatus::findOne($model->apparatus_id);
                    if ($apparatus_model->type = Apparatus::TYPE_COLORED){
                        $model->scenario = $model::SCENARIO_COLORED;
                    } else {
                        $model->scenario = $model::SCENARIO_MONOCHROME;
                    }
                    $model->save();
                    $model->uploadPrintPhoto();

                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'forceClose' => true,
                    ];
                } else {
                    return [
                        'title' => "Редактирование заявки #" . $id,
                        'content' => $this->renderAjax('update', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Repairing model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Repairing model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Repairing model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Repairing the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Repairing::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Присваивает заказу статус "Новый"
     * @param int $id Идентификатор заказа
     * @return array
     * @throws NotFoundHttpException
     * @throws \HttpInvalidParamException
     */
    public function actionToNew($id)
    {
        $order = $this->findModel($id);

        $order->status = $order::STATUS_NEW;

        if (!$order->save()) {
            Yii::error($order->errors, '_error');
        }

        $order->sendNotify();

        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'forceReload' => '#crud-datatable-pjax',
            'title' => 'Отправка заявки',
            'content' => 'Заявка на обслуживание отправлена успешно!',
            'footer' => Html::button('Закрыть',
                ['class' => 'btn btn-default pull-right', 'data-dismiss' => "modal"])
        ];
    }

    /**
     * Скачивание файла
     * @param string $file Путь к файлу
     * @return \yii\console\Response|Response
     * @throws NotFoundHttpException
     */
    public function actionDownload($file)
    {
        if (is_file($file)){
            return Yii::$app->response->sendFile($file);
        } else {
            throw new NotFoundHttpException('Запрашиваемый файл не найден');
        }
    }

    /**
     * Меняет статусы для заявки
     * @param integer $id Идентификатор заявки
     * @param integer $status Статус
     * @return array|Response
     */
    public function actionChangeStatus($id, $status)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $repairing = Repairing::findOne($id) ?? null;

        if (!$repairing) {
            return [
                'title' => 'Ошибка',
                'content' => 'Заявка не найдена'
            ];
        }

        $repairing->status = $status;

        if ($repairing->status == $repairing::STATUS_DONE){
            $repairing->done_at = date('Y-m-d H:i:s', time());
        }

        if (!$repairing->save()){
            Yii::error($repairing->errors, '_error');
        }
        if ($repairing->status == $repairing::STATUS_WORK){
            return $this->redirect('/repairing');
        }

        return [
            'forceReload' => '#crud-datatable-pjax',
            'forceClose' => true,
        ];
    }
}
