<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Apparatus */
?>
<div class="apparatus-view">

    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                [
                    'attribute' => 'brand_id',
                    'value' => $model->brand->name ?? null,
                ],
                'model',
                [
                    'attribute' => 'type',
                    'value' => $model->typeList[$model->type] ?? null,
                ],
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), '_error');
        echo $e->getMessage();
    } ?>

</div>
