<?php

use app\models\User;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset;
use yii\widgets\Pjax;

/** @var User $model */

$this->title = "Профиль";
$this->params['breadcrumbs'][] = $this->title;
CrudAsset::register($this);
$session = Yii::$app->session;
?>

<?php Pjax::begin(['enablePushState' => false, 'id' => 'crud-datatable-pjax']) ?>

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-7">
        <!-- CONTACT ITEM -->
        <div class="panel panel-default">
            <div class="panel-body profile">
                <div class="profile-image">
                    <img src="<?= $model->getAvatar() ?>" style=" width:120px; height:120px; object-fit: cover;"
                         alt="Пользователи">
                </div>
                <div class="profile-data">
                    <div class="profile-data-name"><?= $model->fio ?></div>
                    <div class="profile-data-title"><?= $model->getTypeDescription() ?></div>
                </div>
                <div class="profile-controls">
                    <a href="#" data-toggle="tooltip" class="profile-control-left" title="Уведомления"><span
                                class="fa fa-bell-o" data-pjax="0"></span></a>
                    <a href="/users/change?id=<?= $model->id ?>" data-toggle="tooltip" class="profile-control-right"
                       role="modal-remote" title="Изменить"><span class="fa fa-pencil"></span></a>
                </div>
            </div>
            <div class="panel-body">
                <div class="contact-info">
                    <p>
                        <small>Логин</small>
                        <br><b><?= $model->login ?></b></p>
                    <p>
                        <small>Пользователь Клиента:</small>
                        <br><b><?= $model->client->official_name ?? 'Клиент не найден';?></b></p>
                </div>
            </div>
        </div>
        <!-- END CONTACT ITEM -->
    </div>
</div>

<?php Pjax::end() ?>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer" => "",
]) ?>
<?php Modal::end(); ?>
