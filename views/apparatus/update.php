<?php

/* @var $this yii\web\View */
/* @var $model app\models\Apparatus */
?>
<div class="apparatus-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
