<?php

namespace app\models\query;

/**
 * This is the ActiveQuery class for [[\app\models\Repairing]].
 *
 * @see \app\models\Repairing
 */
class RepairingQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \app\models\Repairing[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\Repairing|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
