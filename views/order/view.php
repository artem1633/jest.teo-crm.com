<?php

use app\models\Apparatus;
use app\models\ApparatusToClient;
use app\models\constants\UsersConstants;
use app\models\Order;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
?>
<div class="order-view">

    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'created_at:datetime',
                [
                    'attribute' => 'client_id',
                    'value' => $model->client ? $model->client->official_name : null,

                ],
                [
                    'attribute' => 'apparatus_id',
                    'value' => function (Order $model) {
                        /** @var Apparatus $apparatus */
                        $apparatus = $model->apparatus ?? null;
                        /** @var \app\models\Client $client */
                        $client = $model->client;
                        $str = '';
                        if ($apparatus) {
                            $str = $apparatus->getApparatusFullName() ?? '';
                        }

                        if ($client) {
                            $str .= ' №' . ApparatusToClient::findOne($model->apparatus_id)->serial_number ?? null;
                        }

                        return $str;
                    },

                ],
                [
                    'attribute' => 'consumables',
                    'value' => function (Order $model){
                        return $model->getConsumablesList(false, ',<br>');
                    },
                    'format' => 'raw'
                ],
                'counter_1',
                [
                    'attribute' => 'counter_2',
                    'visible' => $model->apparatus->type === Apparatus::TYPE_COLORED,
                ],
                [
                    'attribute' => 'counter_3',
                    'visible' => $model->apparatus->type === Apparatus::TYPE_COLORED,
                ],
                [
                    'attribute' => 'done_at',
                    'visible' => UsersConstants::isAdmin(),
                    'format' => 'datetime'
                ],
                [
                    'attribute' => 'photos',
                    'value' => function (Order $model) {
                        return $model->getCountersPhoto();
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'status',
                    'value' => $model->getStatusName($model->status),
                ],
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), '_error');
        echo $e->getMessage();
    } ?>

</div>