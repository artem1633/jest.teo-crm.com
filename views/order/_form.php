<?php

use app\models\ApparatusToClient;
use app\models\Client;
use app\models\constants\UsersConstants;
use app\models\Consumables;
use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */
/* @var $order_to_cons app\models\OrderToConsumables */
/* @var $order_to_cons_models app\models\OrderToConsumables[] */

$counter = 1;
if (UsersConstants::isAdmin()) {
    $client_id = $model->client_id;
} else {
    $client_id = null;
}
Yii::info($model->attributes, 'test');
?>
    <div class="order-form">

        <?php $form = ActiveForm::begin(); ?>

        <?php
        if (UsersConstants::isAdmin()) {
            try {
                echo $form->field($model, 'client_id')->widget(Select2::class, [
                    'data' => (new Client)->getList(),
                    'options' => [
                        'placeholder' => 'Выберите клиента',
                        'disabled' => !$model->isNewRecord,
                    ]
                ]);
            } catch (Exception $e) {
                Yii::error($e->getMessage(), '_error');
                echo $e->getMessage();
            }

            echo $form->field($model, 'status')->dropDownList($model->getStatusList(), ['prompt' => 'Выберите статус']);
        }
        ?>

        <?php
        try {
            echo $form->field($model, 'apparatus_id')->widget(Select2::class, [
                'data' => (new ApparatusToClient)->getApparatusListForClient($client_id),
                'options' => [
                    'id' => 'apparatus-select',
                    'placeholder' => 'Выберите аппарат',
                    'disabled' => !($model->isNewRecord || UsersConstants::isAdmin()),
                ]
            ]);
        } catch (Exception $e) {
            Yii::error($e->getMessage(), '_error');
            echo $e->getMessage();
        } ?>
        <div id="for-clone" class="row" style="display: none; align-items: center;">
            <div class="col-xs-8">
                <?= $form->field($order_to_cons, 'consumables_id[]')->dropDownList((new Consumables)->getList(), [
                    'data-select' => 'consumable',
                    'prompt' => 'Выберите расходик'
                ]) ?>
            </div>
            <div class="col-xs-3">
                <?= $form->field($order_to_cons, 'number[]')->textInput([
                    'maxlength' => true,
                    'id' => 'for-clone',
                    'class' => 'form-control consumables-number',
                ]) ?>
            </div>
            <div class="col-xs-1">
                <?= Html::button('<i class="fa fa-close"></i>', [
                    'class' => 'btn btn-danger btn-block remove-cons-btn',
                ]) ?>
            </div>
        </div>

        <?php if ($model->isNewRecord): ?>
            <div class="consumables-list">
                <div class="row">
                    <div class="col-xs-9">
                        <?= $form->field($order_to_cons,
                            'consumables_id[]')->dropDownList((new Consumables)->getList(), [
                            'data-select' => 'consumable',
                            'id' => 'order-to-consumables-consumables-id-' . $counter,
                            'prompt' => 'Выберите расходник'
                        ]) ?>
                    </div>
                    <div class="col-xs-3">
                        <?= $form->field($order_to_cons, 'number[]')->textInput([
                            'class' => 'form-control consumables-number'
                        ]) ?>
                    </div>
                </div>
            </div>
        <?php else: ?>
            <div class="consumables-list">
                <?php foreach ($order_to_cons_models as $otc_model): ?>
                    <?php
                    if (!isset($otc_model->attributes)) {
                        break;
                    }
                    //Yii::info($otc_model->attributes, 'test'); ?>
                    <div class="row consumable-select" style="display: flex; align-items: center;">
                        <div class="col-xs-8">
                            <?= $form->field($otc_model,
                                'consumables_id[]')->dropDownList((new Consumables)->getList(), [
                                'data-select' => 'consumable',
                                'id' => 'order-to-consumables-consumables-id-' . $counter,
                                'prompt' => 'Выберите расходник',
                                'value' => $otc_model->consumables_id,
                            ]) ?>
                        </div>
                        <div class="col-xs-3">
                            <?= $form->field($order_to_cons, 'number[]')->textInput([
//                                'id' => 'order-to-consumables-number-' . $counter,
                                'class' => 'form-control consumables-number',
                                'value' => $otc_model->number,
                            ]) ?>
                        </div>
                        <div class="col-xs-1">
                            <?= Html::button('<i class="fa fa-close"></i>', [
                                'class' => 'btn btn-danger btn-block remove-cons-btn',
                            ]) ?>
                        </div>
                    </div>
                    <?php
                    $counter++;
                endforeach; ?>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-md-4 col-md-offset-8">
                <?= Html::button('<i class="fa fa-plus"></i> Ещё расходник', [
                    'class' => 'btn btn-block btn-info',
                    'id' => 'add-consumable-btn',
                    'data-counter' => 1,
                    'disabled' => $model->isNewRecord
                ]) ?>
            </div>
        </div>
        <hr>
        <div class="row">
            <?php for ($i = 1; $i < 4; $i++): ?>
                <div data-number="<?= $i; ?>" class="counter-info"
                     style="<?= $i == 1 ? '' : $model->isNewRecord ? 'display:none' : '' ?>">
                    <div class="col-xs-4">
                        <?= $form->field($model, 'counter_' . $i, ['template' => '{input}'])->textInput([
                            'placeholder' => 'Показания счетчика (' . $model->getCounterName($i) . ')',
                            'class' => 'form-control input-counter'
                        ]) ?>
                        <?= $form->field($model, 'counter_' . $i, ['template' => '{error}'])->textInput([
                            'placeholder' => 'Показания счетчика (' . $model->getCounterName($i) . ')'
                        ]) ?>
                    </div>
                </div>
            <?php endfor; ?>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="input-photos">
                    <?php
                    try {
                        echo $form->field($model, 'imageFile',
                            ['template' => '{input}'])->widget(FileInput::class, [
                            'options' => [
                                'accept' => 'image/*',
                                'multiple' => false,
                                'class' => 'image_input',
                                'id' => 'inputFile',
                            ],
                            'pluginOptions' => [
                                'showPreview' => false,
                                'showCaption' => true,
                                'showRemove' => false,
                                'showUpload' => false,
                                'browseClass' => 'btn btn-primary',
                                'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                                'browseLabel' => 'Выберите фото счетчиков',
                                'initialCaption' => $model->photos ? pathinfo($model->photos, PATHINFO_BASENAME) : '',
                                'allowedFileExtensions' => ['jpeg', 'jpg', 'png', 'gif'],
//                                'maxFileCount' => 1,
                            ]
                        ]);
                        echo $form->field($model, 'imageFile', ['template' => '{error}'])->fileInput();
                    } catch (Exception $e) {
                        Yii::error($e->getMessage(), '_error');
                    } ?>
                </div>
            </div>
        </div>
        <?= $form->field($model, 'client_id')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'photos')->hiddenInput()->label(false) ?>
        <?php if ($model->isNewRecord): ?>
            <div class="row">
                <div class="col-md-6 col-md-offset-6">
                    <div class="text-right warning-info">
                        <small class="alert-warning" style="padding: 0.5rem;">Обязательно введите текущие показания
                            счетчиков и приложите их фотографию!
                        </small>
                        <br>
                        <small>(Помощь - <?= Html::a('service@kcepokc.ru', 'mailto:service@kcepokc.ru') ?>)</small>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if (!Yii::$app->request->isAjax) { ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                    ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>

    </div>

<?php
$script = <<<JS
$(document).ready(function() {
    var add_cons_btn = $('#add-consumable-btn');
    var for_clone =  $('#for-clone');
    var counter = $counter;
    var last_consumable;
    var apparatus_type = 1;
    var selected_files = 0;
    var apparatus_select = $('#apparatus-select');
    
    $(document).on('click', '#add-consumable-btn', function(){
        divideOption();
        var elem = for_clone.clone();
        elem.removeAttr('id');
        elem.find('select').attr('id', 'order-to-consumables-consumables-id-' + counter);
        elem.find('input').attr('id', 'order-to-consumables-number-' + counter);
        $('.consumables-list').append(elem);
        elem.slideDown(500);
        elem.css('display', 'flex');
        counter++;
        add_cons_btn.attr('disabled', true);
        console.log(counter);
        elem.unlink();
        return true;
    });
    
    function divideOption(){
        var select = for_clone.find('select option[value="' + last_consumable + '"]');
        select.remove();
    }
    
    $(document).on('change', '[data-select=consumable]', function() {
        last_consumable = $(this).val();
        add_cons_btn.removeAttr('disabled');
    });
    
    $(document).on('change', '#apparatus-select', function() {
        var apparatus = $(this).val();
        var c2 = $('[data-number=2]');
        var c3 = $('[data-number=3]');
        $.get(
            '/order/get-info',
            {id:apparatus},
            function(resp){
                console.log(resp);
                selected_files = 0;
                if (resp.success === 1){
                    apparatus_type = resp.type;
                    if (resp.type === 1){
                        selected_files = 0;
                        c2.fadeOut();
                        c3.fadeOut();
                    } else {
                        c2.fadeIn();
                        c3.fadeIn();
                    }
                    for_clone.find('select').html(resp.data);
                    $('[data-select="consumable"]').html(resp.data);
                    $('.consumables-number').val('');
                } else {
                    alert(resp.error);
                }
            }
        );
        checkInputs();
        return true;
    });
    
    $(document).on('change', '.image_input', function() {
        if ($(this).val()){
            selected_files++;
            checkInputs();
        }
    });
    
    $(document).on('change', '#order-to-consumables-consumables-id-1', function() {
        checkInputs();
    });
    
    $(document).on('click', '#submit-order', function(e) {
        debugger;
        if (checkInputs() !== true){
            e.preventDefault();
            alert('Не заполнены все необходимые поля!')
        }        
    });
    
    $(document).on('focusout', '#order-counter_1 , #order-counter_2, #order-counter_3', function() {
        checkInputs();
    });
    
    $(document).on('click', '.remove-cons-btn', function() {
        var elem = $(this).parent().parent('.row');
        console.log(elem);
        elem.remove();
        $('#add-consumable-btn').removeAttr('disabled');
    });
    
    function checkInputs(){
        console.log('Выбран аппарат: ' + apparatus_select.val());
        console.log('Тип аппарата: ' + apparatus_type);
        console.log('Прикреплено фоток: ' + selected_files);
        
        if ($('#order-to-consumables-consumables-id-1').val() === ''){
            return off();
        }
        
        if (apparatus_select.val() === ''){
            return off();
        }
        
        if (apparatus_type === 1){
            if ($('#order-counter_1').val() === ''){
                return off();   
            }
        } else {
            if ($('#order-counter_1').val() === '' || $('#order-counter_2').val() === '' || $('#order-counter_3').val() === ''){
                return off();   
            }
        }
         if (selected_files < 1){
            return off();
        } else {
            console.log('Выбран файл. Разблокируем кнопку');
            return on()
        }
    }
    
    function off(){
        $('#submit-order').attr('disabled', true);
        $('.warning-info').fadeIn(400);
        return false;
    }
    
    function on(){
        $('#submit-order').removeAttr('disabled');
        $('.warning-info').fadeOut(400);
        return true;
    }
    
});

JS;
$this->registerJs($script);
