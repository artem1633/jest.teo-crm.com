<?php

namespace app\controllers;

use app\models\ApparatusToClient;
use app\models\Order;
use app\models\Repairing;
use app\models\Report;
use app\models\search\OrderSearch;
use app\models\search\RepairingSearch;
use app\models\search\ReportSearch;
use app\models\Statistics;
use app\models\Users;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use kartik\mpdf\Pdf;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function actionPrintlist()
    {
        Yii::$app->response->format = Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');

        $content = $this->renderPartial('print', [

        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}',
            // set mPDF properties on the fly
            'options' => ['title' => 'Krajee Report Title'],
            // call mPDF methods on the fly
            'methods' => [
                'SetHeader' => ['ПЕЧАТЬ РАСЧЕТА'],
                'SetFooter' => ['|{PAGENO}/{nbpg}'],
            ]
        ]);

        return $pdf->render();
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/site/login']);
        }
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string|array
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if (Report::isDebtor()) {
                Yii::$app->session->setFlash('danger',
                    'Вам необходимо предоставить текущие показания счетчиков отпечатков в разделе '
                    . Html::a('"Ежемесячные показания счетчиков"', ['/report']));
            }

            $this->goBack();

        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect(['login']);
    }

    /**
     * @return string|bool
     */
    public function actionAvtorizatsiya()
    {
        /** @var Users $identity */
        $identity = Yii::$app->user->identity ?? null;

        if (isset($identity->id)) {
            return $this->render('error');
        } else {
            Yii::$app->user->logout();
            $this->redirect(['login']);
        }
        return false;
    }

    public function actionSetThemeValues()
    {
        $session = Yii::$app->session;
        if (isset($_POST['sd_position'])) {
            $session['sd_position'] = $_POST['sd_position'];
        }

        if (isset($_POST['header_styling'])) {
            $session['header_styling'] = $_POST['header_styling'];
        }

        if (isset($_POST['sd_styling'])) {
            $session['sd_styling'] = $_POST['sd_styling'];
        }

        if (isset($_POST['cn_gradiyent'])) {
            $session['cn_gradiyent'] = $_POST['cn_gradiyent'];
        }

        if (isset($_POST['cn_style'])) {
            $session['cn_style'] = $_POST['cn_style'];
        }

        if (isset($_POST['boxed'])) {
            $session['boxed'] = $_POST['boxed'];
        }

    }

    public function actionSdPosition()
    {
        $session = Yii::$app->session;
        if ($session['sd_position'] != null) {
            return $session['sd_position'];
        } else {
            return 1;
        }
    }

    public function actionHeaderStyling()
    {
        $session = Yii::$app->session;
        if ($session['header_styling'] != null) {
            return $session['header_styling'];
        } else {
            return 1;
        }
    }

    public function actionSdStyling()
    {
        $session = Yii::$app->session;
        if ($session['sd_styling'] != null) {
            return $session['sd_styling'];
        } else {
            return 1;
        }
    }

    public function actionCnGradiyent()
    {
        $session = Yii::$app->session;
        if ($session['cn_gradiyent'] != null) {
            return $session['cn_gradiyent'];
        } else {
            return 1;
        }
    }

    public function actionCnStyle()
    {
        $session = Yii::$app->session;
        if ($session['cn_style'] != null) {
            return $session['cn_style'];
        } else {
            return 1;
        }
    }

    public function actionBoxed()
    {
        $session = Yii::$app->session;
        if ($session['boxed'] != null) {
            return $session['boxed'];
        } else {
            return 1;
        }
    }

    /**
     * Страница статистики
     * @return string
     */
    public function actionStatistics()
    {
        $request = Yii::$app->request;

        $order_model = new Order();
        $repairing_model = new Repairing();
        $statistics_model = new Statistics();

        $orderSearchModel = new OrderSearch();
        $orderDataProvider = $orderSearchModel->search(Yii::$app->request->queryParams);

        $repairingSearchModel = new RepairingSearch();
        $repairingDataProvider = $repairingSearchModel->search(Yii::$app->request->queryParams);

        $numCopiesSearchModel = new ReportSearch();
        $numCopiesDataProvider = $numCopiesSearchModel->search(Yii::$app->request->queryParams);
        $numCopiesDataProvider->query->orderBy(['report_year' => SORT_DESC, 'report_month' => SORT_DESC]);

        $stat_info = [];
        $stat_info['order_count_all'] = Order::find()->count();
        $stat_info['order_count_done'] = Order::find()->andWhere(['status' => $order_model::STATUS_DONE])->count();
        $stat_info['repairing_count_all'] = Repairing::find()->count();
        $stat_info['repairing_count_done'] = Repairing::find()->andWhere(['status' => $repairing_model::STATUS_DONE])->count();

        if ($request->isPost) {
            //Обрабатываем значения фильтров
            $statistics_model->load($request->post());

            if (!$statistics_model->date_start) {
                //Если начальная дата не указана устанавливаем период - все
                $statistics_model->date_start = '2000-01-01 00:00:00';
                $statistics_model->date_end = date('Y-m-d 23:59:59', time()); //  strtotime("last day of last month")
            }

            if (!$statistics_model->date_end) {
                //Если не указан конец периода - выставляем текущую дату
                $statistics_model->date_end = date('Y-m-d 23:59:59', time());
            }

            Yii::info($statistics_model->attributes, 'test');

            if ($statistics_model->on_created_at) {
                $date_field = 'created_at';
            } else {
                $date_field = 'done_at';
            }

            $orderDataProvider->query
                ->andFilterWhere(['status' => $statistics_model->status])
                ->andFilterWhere(['client_id' => $statistics_model->client_id])
                ->andFilterWhere(['BETWEEN', $date_field, $statistics_model->date_start, $statistics_model->date_end]);
            $repairingDataProvider->query
                ->andFilterWhere(['status' => $statistics_model->status])
                ->andFilterWhere(['client_id' => $statistics_model->client_id])
                ->andFilterWhere(['BETWEEN', $date_field, $statistics_model->date_start, $statistics_model->date_end]);

            if ($statistics_model->date_start != '2000-01-01 00:00:00'){
                $cur_month = date('m', strtotime($statistics_model->date_start));
                $cur_year = date('Y', strtotime($statistics_model->date_start));
            } else {
                $cur_month = null;
                $cur_year = null;
            }

            $numCopiesDataProvider->query
                ->andFilterWhere(['status' => $statistics_model->status])
                ->andFilterWhere(['client.id' => $statistics_model->client_id])
                ->andFilterWhere(['report.report_month' => $cur_month])
                ->andFilterWhere(['report.report_year' => $cur_year]);
        } else {
            $statistics_model->on_created_at = 1;
        }

        return $this->render('statistics', [
            'stat_info' => $stat_info,
            'statistics_model' => $statistics_model,
            'orderDataProvider' => $orderDataProvider,
            'repairingDataProvider' => $repairingDataProvider,
            'numCopiesDataProvider' => $numCopiesDataProvider,
        ]);
    }

    public function actionSetLeftMenu($current_class)
    {
        return $current_class;
    }

    public function actionHelp()
    {
        return $this->render('help');
    }

    /**
     * Метод исправляющий косяк отношений аппаратов и клиентов
     */
    public function actionCorrectDataBase()
    {
        //Косяк: В заявки запысывался ID аппарата (apparatus) вместо ID аппарата клиента (apparatus_to_client)
        //Инциденты
        $repairs = Repairing::find()->all();
        Yii::info('Repairs count: ' . count($repairs), 'test');

        /** @var Repairing $repair */
        foreach ($repairs as $repair) {
            //Ищем аппарат клиента
            $atc = ApparatusToClient::find()->andWhere([
                'apparatus_id' => $repair->apparatus_id,
                'client_id' => $repair->client_id
            ])->one();

            //меняем ID аппарата на ID аппарата клиента
            $repair->apparatus_id = $atc->id;
            $repair->save(false);
        }
        //Заказы расходников
        $orders = Order::find()->all();
        Yii::info('Orders count: ' . count($orders), 'test');

        /** @var Order $order */
        foreach ($orders as $order) {
            //Ищем аппарат клиента
            $atc = ApparatusToClient::find()->andWhere([
                'apparatus_id' => $order->apparatus_id,
                'client_id' => $order->client_id
            ])->one();

            //меняем ID аппарата на ID аппарата клиента
            $order->apparatus_id = $atc->id;
            $order->save(false);
        }
    }
}

