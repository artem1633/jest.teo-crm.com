<?php

use app\models\Apparatus;
use app\models\Brand;
use kartik\select2\Select2;
use kartik\switchinput\SwitchInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApparatusToClient */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="apparatus-to-client-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">
            <?php
            try {
                echo $form->field($model, 'brand')->widget(Select2::class, [
                    'data' => (new Brand())->list,
                    'options' => [
                        'placeholder' => 'Выберите марку'
                    ],
                    'pluginEvents' => [
                        "change" => "function() {
                            $.get(
                                '/apparatus/get-list-for-brand',
                                {id:$(this).val()},
                                function (response){
                                console.log(response.error);
                                    if (response.success === 1){
                                        console.log(response);
                                        var select_model = $('#apparatus-model'); 
                                        select_model.html(response.data);
                                        select_model.attr('disabled', false)
                                    } else {
                                       alert(response.error.content);
                                    }
                                }
                            )
                        }",
                    ]
                ])->label('Марка');
            } catch (Exception $e) {
                Yii::error($e->getMessage(), '_error');
                echo $e->getMessage();
            } ?>
        </div>
        <div class="col-md-8">
            <?php
            try {
                echo $form->field($model, 'apparatus_id')->widget(Select2::class, [
                    'data' => (new Apparatus())->list,
                    'options' => [
                        'id' => 'apparatus-model',
                        'disabled' => true,
                        'placeholder' => 'Выберите модель'
                    ]
                ])->label('Модель');
            } catch (Exception $e) {
                Yii::error($e->getMessage(), '_error');
                echo $e->getMessage();
            } ?>
        </div>
    </div>


    <?= $form->field($model, 'notes')->textarea(['rows' => 6]) ?>

    <div class="row">
        <div class="col-md-4">
            <?php
            try {
                echo $form->field($model, 'is_owner')->widget(SwitchInput::class, [
                    'pluginOptions' => [
                        'onText' => 'Да',
                        'offText' => 'Нет',
                    ]
                ]);
            } catch (Exception $e) {
                Yii::error($e->getMessage(), '_error');
                echo $e->getMessage();
            } ?>
        </div>
        <div class="col-md-8">
            <?= $form->field($model, 'serial_number')->textInput(['maxlength' => true]) ?>
        </div>
    </div>


    <?= $form->field($model, 'client_id')->hiddenInput()->label(false) ?>

    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
