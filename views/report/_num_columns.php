<?php

use app\models\constants\UsersConstants;
use app\models\Report;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'report_month',
        'filter' => (new Report())->getMonthList(),
        'content' => function (Report $model) {
            return $model->getMonthName($model->report_month) . ', ' . $model->report_year;
        },
        'label' => 'Месяц',
        'hAlign' => 'center',
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'client_name',
        'content' => function (Report $model) {
            if ($model->apparatusToClient) {
                return $model->apparatusToClient->client ? $model->apparatusToClient->client->official_name : null;
            }
        },
        'visible' => UsersConstants::isAdmin(),
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'apparatus_full_name',
        'content' => function (Report $model) {
            $name = $model->apparatus ? $model->apparatus->getApparatusFullName() : '';
            $sn = $model->apparatusToClient ? $model->apparatusToClient->serial_number : '';
            $apparatus = $name . ' (№&nbsp;' . $sn . ')';
            return $apparatus;
        },
        'hAlign' => 'center',
        'vAlign' => 'middle',
    ],

    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'counter_1',
        'content' => function (Report $model) {
            return $model->getNumCopiesPerMonth();
        },
        'label' => 'Сделано копий',
//        'hAlign' => 'center',
//        'vAlign' => 'middle',
    ],
];