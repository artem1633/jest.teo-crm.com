<?php

use app\models\constants\UsersConstants;
use app\models\Order;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

$export =  UsersConstants::isSuperAdmin() ? '{export}' : '';

?>
<div class="panel panel-inverse order-index">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                        class="fa fa-expand"></i></a>
            <a href="javascript:" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                        class="fa fa-repeat"></i></a>
            <a href="javascript:" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i
                        class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Заказы расходных материалов</h4>
    </div>
    <div class="panel-body">
        <div id="ajaxCrudDatatable">
            <?php
            try {
                echo GridView::widget([
                    'id' => 'crud-datatable',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'rowOptions' => function (Order $model, $key, $index, $grid) {
                        if (UsersConstants::isAdmin() && $model->status != $model::STATUS_DONE){
                            $class = ' danger';
                        } else {
                            $class = '';
                        }
                        return [
                            'class' => 'crud-datatable' . $class,
                        ];
                    },
                    'tableOptions' => ['class' => 'table table-bordered table-hover'],
                    'pjax' => true,
                    'columns' => require(__DIR__ . '/_columns.php'),
                    'responsiveWrap' => false,
                    'toolbar' => [
                        [
                            'content' =>
                                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
                                    [
                                        'role' => 'modal-remote',
                                        'title' => 'Добавить аппарат',
                                        'class' => UsersConstants::isAdmin() ? 'hidden' : 'btn btn-default',
                                    ]) .
                                '{toggleData}' .
                                $export
                        ],
                    ],
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'panel' => [
                        'headingOptions' => ['style' => 'display: none;'],
                        '<div class="clearfix"></div>',
                    ]
                ]);
            } catch (Exception $e) {
                Yii::error($e->getMessage(), '_error');
                echo $e->getMessage();
            } ?>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

<?php
$script = <<<JS
$(document).on('click', '.order-column', function() {
    var href = $(this).parent('.crud-datatable').find('.view-repairing');
    setTimeout(function(){
        href.trigger('click');
    }, 100);
})
JS;

$this->registerJs($script);

try {
    $this->registerJsFile('/plugins/lightbox/js/lightbox.min.js');
} catch (\yii\base\InvalidConfigException $e) {
    echo $e->getMessage();
}