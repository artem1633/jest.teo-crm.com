<?php

use kartik\file\FileInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Client */
/* @var $form yii\widgets\ActiveForm */
/* @var $contract_model \app\models\Contract */

$file_info = [];
if (isset($file_information)) {
    $file_info = $file_information;
}

Yii::info($file_info, 'test');
?>

<div class="client-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'official_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
    </div>


    <?= $form->field($model, 'address')->textarea(['rows' => 2]) ?>
    <div class="row">

        <div class="col-md-6">
            <?= $form->field($model, 'contact_person')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'phone_number')->textInput() ?>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <p>Информация о договоре</p>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($contract_model, 'number')->textInput() ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($contract_model, 'date')->input('date') ?>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <?php
            try {
                echo $form->field($model, 'bank_details_file')->widget(FileInput::class, [
                    'options' => [
                        'multiple' => false,

                    ],
                    'pluginOptions' => [
                        'showCaption' => false,
                        'showRemove' => false,
                        'showUpload' => false,
                        'browseClass' => 'btn btn-primary btn-block',
                        'browseIcon' => '<i class="glyphicon glyphicon-open-file"></i> ',
                        'browseLabel' => $model->bank_details_file ? 'Выберите файл для замены' : 'Выберите файл',
                        'showPreview' => !$model->bank_details_file,
                        'uploadUrl' => Url::to(['@web/' . $model->bank_details_file]),
                        'initialPreview' => isset($file_info['path']) ? [$file_info['path']] : false,
                        'allowedFileExtensions' => ['doc', 'docx', 'txt', 'odt', 'pdf'],
                    ]
                ]);
            } catch (Exception $e) {
                Yii::error($e->getMessage(), '_error');
                echo $e->getMessage();
            } ?>
        </div>
    </div>
    <?= $form->field($model, 'contract_path')->hiddenInput()->label(false)->hint(false) ?>

    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
